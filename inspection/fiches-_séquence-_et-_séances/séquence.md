# Fiche de séquence : module 8

<p class="h1 text-center pb-4" style="font-size:1.5rem;text-align: center !important;
text-align-last: center;">Représentation des entiers relatifs et réels en binaire et tri et fusion de données dans une table</p>

<style>
#lessonBlocHTML-content table th{
    text-align:center;
}
#lessonBlocHTML-content p, #lessonBlocHTML-content li { 
  text-align: justify;
  text-align-last: left;
}
#lessonBlocHTML-content h1{
    padding-bottom: 0.5rem !important;
}
</style>

<table class="table w-100">
    <tbody>
        <tr>
            <td><b>Place dans l'année scolaire : </b> mois de février (cours / activités) et mars (projet)</td>
        </tr>
        <tr>
            <td><b>Place dans la progression : </b> semaines 6 => 12</td>
        </tr>
        <tr>
            <td><b>Nombre de séance(s) : </b><ul>
              <li>cours / activités : 2 séances (2 * 2h)</li>
              <li>projet : 3 séances (3 * 2h) + 2 séances (2 * 2h) car les séances tampons du deuxième trimestre n'ont pas été prises</li>
            </ul></td>
        </tr>
    </tbody>
</table>

+++sommaire

## Présentation générale de la séquence

Cette séquence aborde des concepts appartenant à deux thèmes du programme : 

- **Représentation des données: types et valeurs de base**
- **Traitement de données en tables**

Contrairement à d'autres séquences antérieures, les liens ici entre ces deux thèmes sont assez faibles. Leur placement dans la progression s'inscrit avant tout dans une approche pédagogique dite **spiralée**.

Ainsi, nous avons déjà travaillé sur ces deux thèmes. Cette séquence permet alors de réactiver des concepts auparavant introduits et d'aller un peu plus loin dans les attendus du programme.

À l'issue de cette séquence, nous terminons avec les élèves un des thème du programme : "traitement de données en tables". 

**Un grand projet évalué** démarre à la sortie des vacances de février et s'étalera sur 5 séances. Le fond du projet consiste à développer un mini-site Web dont le contenu se base sur un fichier de données CSV. Ce projet permet d'évaluer une partie du contenu couvert dans cette séquence. La partie restante sera évaluée ultérieurement à travers un devoir surveillé.


## La séquence en détail

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Capacité(s) visée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr><td>
          <p class="mb-0"><b>Thème :</b> représentation des données: types et valeurs de base</p>
          <ul>
            <li>Utiliser le complément à 2</li>
            <li>Calculer sur quelques exemples la représentation de nombres réels : 0.1, 0.25 ou 1/3</li>
          </ul>
          <p class="mb-0"><b>Thème :</b> traitement de données en tables</p>
          <ul>
            <li>Trier une table suivant une colonne</li>
            <li>Construire une nouvelle table en combinant les données de deux tables</li>
          </ul>
        </td></tr>
    </tbody>
</table>

<table class="table table-sm">
    <thead>
        <tr>
            <th>Compétence(s) travaillée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">
              <p class="mb-0"><b>Compétences de discipline :</b></p>
              <ul class="mb-0">
                  <li>Analyser un problème en termes de flux et de traitement d’informations</li>
                  <li>Décomposer un problème en sous-problèmes, reconnaître des situations déjà analysées</li>
                  <li>Mobiliser les concepts et les technologies utiles pour assurer les fonctions de mémorisation et de traitement des informations</li>
                  <li>Développer des capacités d’abstraction et de généralisation</li>
              </ul>
              <p class="mb-0 mt-2"><b>Compétences transversales :</b></p>
              <ul class="mb-0">
                  <li>Faire preuve d’autonomie, d’initiative et de créativité</li>
                  <li>Présenter un problème ou sa solution</li>
                  <li>Coopérer au sein d’une équipe</li>
                  <li>Rechercher de l’information, apprendre à utiliser des sources de qualité, partager des ressources</li>
              </ul>
            </td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Aperçu des séances</th>
        </tr>
    </thead>
    <tbody>
        <tr><td>
          <p><b><u>Cours et activités</u></b></p>
          <p class="mb-0"><b>Séance n°0</b> : <a href="/enseignant/inspection/fiches-_sequence-_et-_seances/seances/seance_0.md" target="_blank"><i class="isFile fas fa-file-alt"></i> lien vers la fiche séance</a></p>
          <ul>
            <li>nous réinvestissons sur les concepts de représentation de nombre binaire côté machine et en profitons pour aller un peu plus loin : comment une machine traite des nombres négatifs et des nombres réels ?</li>
          </ul>
          <p class="mb-0"><b>Séance n°1</b> : <a href="/enseignant/inspection/fiches-_sequence-_et-_seances/seances/seance_1.md" target="_blank"><i class="isFile fas fa-file-alt"></i> lien vers la fiche séance</a></p>
          <ul>
            <li>nous continuons le travail initié sur les fichiers CSV quelques séances auparavant. Entre temps nous avons abordé les algorithmes de tri, donc nous allons jouer avec les deux et voir comment trier des données en table. Le contenu de cette séance permet de préparer le projet qui s'ensuit</li>
          </ul>
          <p><b><u>Projet</u></b></p>
          <p class="mb-0"><b>Séance n°2</b> : <a href="/enseignant/inspection/fiches-_sequence-_et-_seances/seances/seance_2_projet.md" target="_blank"><i class="isFile fas fa-file-alt"></i> lien vers la fiche séance</a></p>
          <ul>
            <li>Première séance consacrée au projet. Les sujets choisis parmi la liste de sujets proposés sont présentés et les documents utiles sont fournis aux élèves. À l'occasion de l'introduction de ce nouveau projet, les élèves sont informés de l'existence du concours "trophées NSI" et du fait que la présentation orale peut être adaptée pour servir à préparer ce concours. Lors de cette séance, j'interviens peu dans l'organisation intra-groupe.</li>
          </ul>
          <p class="mb-0"><b>Séance n°3</b> : <a href="/enseignant/inspection/fiches-_sequence-_et-_seances/seances/seance_3_projet.md" target="_blank"><i class="isFile fas fa-file-alt"></i> lien vers la fiche séance</a></p>
          <ul>
            <li>Deuxième séance en mode "projet". Un nouveau rituel est mis en place avec les élèves : le daily meeting. Des remarques et consignes peuvent être données suite aux observations que j'ai pu faire lors de la première séance.J'accompagne les groupes dans leur projet mais de manière moins appuyée que lors de la première séance (les objectifs sont de favoriser l'autonomie et l'entre aide entre groupes)</li>
          </ul>
          <p class="mb-0"><b>Séance n°4</b> : <a href="/enseignant/inspection/fiches-_sequence-_et-_seances/seances/seance_4_projet.md" target="_blank"><i class="isFile fas fa-file-alt"></i> lien vers la fiche séance</a></p>
          <ul>
            <li>Troisième séance en mode "projet". Reprends dans les grandes lignes le même contenu que la séance précédente.</li>
          </ul>
          
          <fieldset class="border border-1 p-2 mb-4">
             <legend class="float-none w-auto"><span class="text-blue"><del>[séance inspectée]</del> (covid)</span></legend>
             <p class="mb-0"> <b>Séance n°5</b> : <a href="/enseignant/inspection/fiches-_sequence-_et-_seances/seances/seance_5_projet.md" target="_blank"><i class="isFile fas fa-file-alt"></i> lien vers la fiche séance</a></p>
            <ul>
              <li>Quatrième séance en mode "projet". Reprends dans les grandes lignes le même contenu que la séance précédente en ajoutant une nouvelle consigne : les groupes ne peuvent me solliciter qu'au plus deux fois durant la séance pour demander de l'aide. Cette consigne est donnée et détaillée lors du daily meeting. Je m'autorise toutefois d'intervenir auprès d'un groupe ou d'un élève de ma propre initiative</li>
            </ul>
          </fieldset>
          
          <fieldset class="border border-4 p-2 mb-4">
             <legend class="float-none w-auto"><b class="text-blue">[MAJ] [séance inspectée]</b></legend>
          <p class="mb-0"><b>Séance n°6</b> : <a href="/enseignant/inspection/fiches-_sequence-_et-_seances/seances/seance_6_projet.md" target="_blank"><i class="isFile fas fa-file-alt"></i> lien vers la fiche séance</a></p>
          <ul>
            <li>Cinquième et (normalement) dernière séance en mode "projet". Reprends dans les grandes lignes le même contenu que la séance précédente. À la suite de cette séance, les élèves auront une semaine pour finaliser leur projet et préparer leur oral de présentation.</li>
          </ul>
          </fieldset>
        </td></tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Évaluation(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr><td>
          <p class="mb-0"><b>Diagnostique : </b></p>
          <ul>
            <li>En début de chaque séances, un premier exercice et/ou des échanges sont faits avec les élèves pour réactiver des concepts qui sont des pré-requis pour la séance. Je bénéficie ainsi des avantages de ma progression spiralée. Ce début de cours me permet d'adapter le temps qui devra être pris collectivement et individuellement avec les élèves dans les activités qui suivent</li>
          </ul>
          <p class="mb-0"><b>Formative : </b></p>
          <ul>
            <li>Les activités effectuées, ainsi que leur correction, lors de la séance permet aux élèves de se situer dans leurs apprentissages. Des interrogations orales (volontaires ou désignées) ponctuent les séances (hors projet)</li>
          </ul>
          <p class="mb-0"><b>Sommative : </b></p>
          <ul>
            <li>Le projet permettra d'évaluer une partie des capacités abordées dans cette séquence (+++lien "/enseignant/didactique/modeles/projet/grille-_evaluation-_projet.md" "grille d'évaluation de projet"). Le reste sera évalué dans le cadre +++lien "/hidden/controles/01-__-_NSI-_premiere/NSImodule09_2021_2022.md" "d'un devoir surveillé" <i class="fas fa-lock text-red"></i> prévu à l'issue du module 9</li>
          </ul>
        </td></tr>
    </tbody>
</table>

## Analyse réflexive

### Phase de conception de la séquence 

Cette séquence fait suite à la septième, une des plus difficiles du programme pour les élèves, qui était essentiellement consacrée à l'algorithmique.

Ainsi dans cette huitième séquence, il m'a semblé important de nous éloigner de concepts parfois abstraits et parfois mathématiques (voir très mathématiques pour certains élèves) afin de nous rapprocher d'activités davantage concrètes. Pour accompagner et concrétiser cette transition, un projet conséquent attends les élèves en deuxième partie de cette séquence. Il faut alors que je m'assure que mes élèves soient suffisamment équipés pour pouvoir développer le projet dans de bonnes conditions.

Mesurant les difficultés que pouvaient rencontrer mes élèves en se confrontant à la validité des algorithmes de tri, j'ai alors choisi de placer au début de cette huitième séquence une partie du programme où des mathématiques, plus abordables, sont sollicités. Tout en restant dans une logique de progression spiralée, ce début de séquence fait suite à des notions que nous avons travaillées plus tôt dans l'année tout en permettant à des élèves en difficulté de reprendre confiance vis à vis des mathématiques qui reste un des fondements de l'informatique. 

La deuxième séance de la séquence est une mise à l'étrier pour le projet qui s'en suit. Le but du projet sera d'afficher, à l'aide d'une interface graphique Web, le contenu d'un fichier CSV et de permettre à l'internaute de trier les données affichées sur le site. Il s'agira jusqu'à lors du projet le plus long effectué par les élèves. Il est donc important d'initier les élèves sur certains aspects techniques liés au projet mais également (et surtout ?) de les accompagner dans leur prise d'autonomie et dans le travail en équipe.

### Bilan de séquence

La séquence n'est pas terminée au moment où j'écris ces lignes, mais je peux déjà apporter quelques éléments dans ma réflexion.

#### <b>Les <span class="text-green">plus</span></b>

- Les capacités prévues dans cette séquence ont toutes été travaillées et dans les temps prévus
- La classe évolue dans un climat propice au travail dans l'ensemble
- Le temps dédié aux activités occupe la majeure partie des séances, les élèves sont actifs la plupart du temps
- Les activités sont adaptées en fonction des niveaux des élèves :
    - du contenu est prévu pour les élèves en avance, parfois j'invite ces élèves à venir épauler les autres
    - je peux consacrer un temps important auprès des élèves en difficulté. Ce qui me permet entre autres de prendre en compte les différences de niveau et d'évaluer l'ensemble de la classe.
- Nous profitons des avantages d'une progression spiralée. Les quelques rappels en début de séance restent utiles pour une partie des élèves mais la plupart ont pu entamer la première activité des deux premières séances sans trop de difficultés. Ce qui les met en confiance pour la suite. 

#### <b>Les <span class="text-red">moins</span></b>

- Peu de devoir à la maison, entraînant un faible suivi "hors séance" des élèves en difficulté
- Certains moments de relâchement collectif (notamment le mardi de 17h à 18h avec le groupe 1, le mardi est une journée très dense pour les élèves)
- Certains rares élèves se laissent par moment porter par les autres et/ou attendent les réponses
- Les évaluations formatives restent assez informelles

#### **Points de remédiations**

**Pour cette séquence en particulier :**

Je suis pour l'instant assez satisfait du contenu de la séquence et de son déroulement avec les élèves. Une chose a changé par rapport aux séquences précédentes : 
habituellement, des corrections aux activités en classes sont disponibles. J'invite parfois certains élèves à aller les consulter pour vérifier par exemple leurs réponses en cours d'activité. Dans cette séquence, je n'ai pas pu prendre le temps de rédiger les corrections pour l'activité de découverte de la bibliothèque python pandas. En l'absence de corrigé, certains élèves se sont parfois spontanément levés pour aller donner des explications ou alors en chercher, ce qui instaure un climat de classe fort agréable ! et très propice à du travail en mode projet.

Suite à cette observation, selon le contexte dans lequel s'inscrit l'activité en question, je m'interrogerai davantage sur la mise à disposition, ou non, de corrigés avec mes activités.

**De manière plus générale :**

Je profite cette année des avantages du petit effectif d'élèves que j'ai par classe de première (2 groupes ; un de 16 et un de 18 élèves). Ce qui me permet d'évaluer assez facilement ces derniers et de mieux accompagner en conséquence les élèves en difficulté. Je complète par ailleurs ces accompagnements ponctuels par des interrogations orales sur la base du volontariat ou par désignation.
Cependant, je me rends compte qu'avec un plus fort effectif, le système que j'utilise risque d'atteindre ses limites. Je réfléchis actuellement sur la mise en place de quiz collectifs intervenants ponctuellement lors des séances. (en m'aidant d'outils déjà disponibles ou en développant le miens sur ma plateforme edusophie)

Le niveau général des deux groupes de NSI est bon, voir très bon. Cependant, quelques rares élèves semblent avoir abandonné la spécialité avant l'heure. Je dois passer souvent derrière ces élèves pour les motiver en cours de séance. Les raisons sont multiples et sont antérieures à cette séquence. Cependant je me rends compte que l'ajout de devoirs à faire à la maison n'aurait pas été de trop pour certains. Lors de la conception de la progression et des supports de cours de cette année, je partais du postulat qu'un étudiant qui choisit la spécialité et qui est en première sera déjà assez autonome (d'autant plus que les études supérieures arrivent dans moins de deux ans). Je vois bien à travers cette première année que certains élèves ont besoin d'évoluer / de progresser dans un environnement assez cadré. Après réflexions, la solution pour l'instant qui me paraît la plus simple et efficace serait de mettre à disposition des exercices à faire chez soi, ceci couplé avec un système de point bonus pour inciter les élèves à les faire.