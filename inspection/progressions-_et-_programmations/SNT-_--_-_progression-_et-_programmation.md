# SNT : progression et programmation

+++sommaire

## Liste des thèmes

<style>
  .t0{color:#e114dc}
  .t1{color:#67b021}
  .t2{color:#069668}
  .t3{color:#b25ea8}
  .t4{color:#081394}
  .t5{color:#932226}
  .t6{color:#444444}
  
  .cancel{background-color:#FFA3A3}
  
  
  .modEven{background-color:#baffab !important;vertical-align: middle}
  .modOdd{background-color:#abfbff !important;vertical-align: middle}
  .vac{background-color:#ffabfb !important;vertical-align: middle}
  
  table td ul{
    margin-bottom:0px;
  }
  /*
  thead > tr {
    position: sticky;
    top: 0;
    background-color: #edecec;
    box-shadow: 0 4px 2px -1px rgba(0, 0, 0, 0.4);
  }
  */
  table {
    border-collapse: separate; /* Don't collapse */
    border-spacing: 0;
  }
  
</style>

<ol start="0">
  <li class="t0">La photographie numérique</li>
  <li class="t1">Les données structurées et leur traitement</li>
  <li class="t2">Localisation, cartographie et mobilité</li>
  <li class="t3">Internet</li>
  <li class="t4">Le web</li>
  <li class="t5">Les réseaux sociaux</li>
  <li class="t6">Informatique embarquées et objets connectés</li>
</ol>

Le thème 'Notions transversales de programmation' étant transverse. Il n'est pas détaillé dans le tableau de la progression. 

## Progression

Les thèmes sont abordés les uns après les autres au fil de l'année. Chacun des thème occupent en moyenne 4 séances d'une heure. Le temps restant occupe des séances d'une heure en demis groupes tous les 15 jours. Dans ces séances nous faisons des exercices python pour accompagner le fil conducteur de l'année.

Les évaluations sont organisées en dehors des heures de cours de SNT

<ol start="0">
  <li class="t0">La photographie numérique</li>
  <li class="t1">Les données structurées et leur traitement</li>
  <li class="t2">Localisation, cartographie et mobilité</li>
  <li class="t3">Internet</li>
  <li class="t4">Le web</li>
  <li class="t5">Les réseaux sociaux</li>
  <li class="t6">Informatique embarquées et objets connectés</li>
</ol>

## Programmation

<table class="table">
<thead>
<tr>
    <th scope="col" class="text-vertical px-0 text-center">semaine</th>
    <th scope="col" class="text-vertical px-0 text-center">séance</th>
    <th class="w-100 align-middle text-center" scope="col">contenu</th>
</tr>
</thead>
<tbody>
  <tr><td class="modEven" colspan="3"><div class="float-start"><b>[ Thème 0 : <span class="t0">La photographie numérique</span> ]</b> ( <a target="_blank" href="/apprenant/00-__-_SNT/00-__-_la-_photographie-_numerique"><i class="fa fa-folder isDirectory"></i></a> )</div></td></tr>
  <tr><td>36</td><td>0</td>
    <td>
    <ul>
      <li class="t0">Distinguer les photosites du capteurs et les pixels de l'image en comparant les résolutions du capteur et de l'image selon les réglages de l'appareil</li>
    </ul></td>
  </tr>
  <tr><td>37</td><td>1</td>
    <td><ul>
      <li class="t0">Traiter par programme une image pour la transformer en agissant sur les 3 composantes de ses pixels</li>
    </ul></td>
  </tr>
    <tr><td>38</td><td>2</td>
    <td><ul>
      <li class="t0">Expliciter des algorithmes associés à la prise de vue</li>
      <li class="t0">Identifier les étapes de la construction de l'image finale</li>
    </ul></td>
  </tr><tr><td>39</td><td>3</td>
    <td><ul>
      <li class="t0">Retrouver les méta-données d'une photographie</li>
    </ul></td>
  </tr>
  <tr><td class="modOdd" colspan="3"><div class="float-start"><b>[ Thème 1 : <span class="t1">Les données structurées et leur traitement</span> ]</b> ( <a target="_blank" href="/apprenant/00-__-_SNT/01-__-_les-_donnees-_structurees-_et-_leur-_traitement"><i class="fa fa-folder isDirectory"></i></a> )</div></td></tr>
  <tr class="cancel"><td>40</td><td></td>
    <td><ul>
      <li>Journée pédagogique</li>
    </ul></td>
  </tr>
  <tr><td>41</td><td>0</td>
    <td><ul>
      <li class="t1">Identifier les différents descripteurs d’un objet</li>
      <li class="t1">Retrouver les méta-données d’un fichier personnel</li>
      <li class="t1">Distinguer la valeur d’une donnée de son descripteur</li>
    </ul></td>
  </tr>
  <tr><td>42</td><td>1</td>
    <td><ul>
      <li class="t1">Identifier les principaux formats et représentations de données</li>
      <li class="t1">Utiliser un site de données ouvertes, pour sélectionner et récupérer des données</li>
      <li class="t1">Réaliser des opérations de recherche, filtre, tri ou calcul sur une ou plusieurs tables</li>
    </ul></td>
  </tr>
  <tr><td colspan="3" class="vac"><b>Vacances de Toussaint</b></td></tr>
  <tr><td>45</td><td>2</td>
    <td><ul>
      <li class="t1">Identifier les principaux formats et représentations de données</li>
      <li class="t1">Utiliser un site de données ouvertes, pour sélectionner et récupérer des données</li>
      <li class="t1">Réaliser des opérations de recherche, filtre, tri ou calcul sur une ou plusieurs tables</li>
    </ul></td>
  </tr>
  <tr><td>46</td><td>3</td>
    <td><ul>
      <li class="t1">Définir une donnée personnelle</li>
      <li class="t1">Utiliser un support de stockage dans le nuage</li>
      <li class="t1">Partager des fichiers</li>
    </ul></td>
  </tr>
  <tr><td class="modOdd" colspan="3"><div class="float-start"><b>[ Thème 2 : <span class="t2">Localisation, cartographie et mobilité</span> ]</b> ( <a target="_blank" href="/apprenant/00-__-_SNT/02-__-_localisation---_-_cartographie-_et-_mobilite"><i class="fa fa-folder isDirectory"></i></a> )</div></td></tr>
  <tr><td>47</td><td>0</td>
    <td><ul>
      <li class="t2">Décrire le principe de fonctionnement de la géolocalisation</li>
      <li class="t2">Utiliser un logiciel pour calculer un itinéraire</li>
      <li class="t2">Représenter un calcul d’itinéraire comme un problème sur un graphe</li>
    </ul></td>
  </tr>
  
    <tr class="cancel"><td>48</td><td></td>
    <td><ul>
      <li>Contrôles trimestriels</li>
    </ul></td>
  </tr>
  <tr><td>49</td><td>1</td>
    <td><ul>
      <li class="t2">Contribuer à OpenStreetMap de façon collaborative.</li>
      <li class="t2">Identifier les différentes couches d’information de GeoPortail pour extraire différents types de données</li>
    </ul></td>
  </tr>
  <tr><td>50</td><td>2</td>
    <td rowspan="2"><ul>
      <li class="t2">Décrire le principe de fonctionnement de la géolocalisation</li>
      <li class="t2">Décoder une trame NMEA pour trouver des coordonnées géographiques.</li>
    </ul></td>
  </tr>
  <tr><td>51</td><td>3</td>
  </tr><tr><td colspan="3" class="vac"><b>Vacances de Noël</b></td></tr>
  <tr><td class="modOdd" colspan="3"><div class="float-start"><b>[ Thème 3 : <span class="t3">Internet</span> ]</b> ( <a target="_blank" href="/apprenant/00-__-_SNT/03-_Internet"><i class="fa fa-folder isDirectory"></i></a> )</div></td></tr>
  <tr><td>2</td><td>0</td>
    <td rowspan="3"><ul>
      <li class="t3">Distinguer le rôle des protocoles IP et TCP</li>
      <li class="t3">Caractériser les principes du routage et ses limites</li>
      <li class="t3">Distinguer la fiabilité de transmission et l’absence de garantie temporelle. </li>
      <li class="t3">Caractériser quelques types de réseaux physiques : obsolètes ou actuels, rapides ou lents, filaires ou non.</li>
      <li class="t3">Caractériser l’ordre de grandeur du trafic de données sur internet et son évolution.</li>
    </ul></td>
  </tr>
  <tr><td>3</td><td>1</td>
  </tr>
  <tr><td>4</td><td>2</td>
  </tr>
  <tr><td>5</td><td>3</td>
    <td><ul>
      <li class="t3">Sur des exemples réels, retrouver une adresse IP à partir
d’une adresse symbolique et inversement</li>
      <li class="t3">Décrire l’intérêt des réseaux pair-à-pair ainsi que les usages
illicites qu’on peut en faire</li>
    </ul></td>
  </tr>
  <tr><td class="modOdd" colspan="3"><div class="float-start"><b>[ Thème 4 : <span class="t4">Web</span> ]</b> ( <a target="_blank" href="/apprenant/00-__-_SNT/04-_le-_Web"><i class="fa fa-folder isDirectory"></i></a> )</div></td></tr>
  <tr><td>7</td><td>0</td>
    <td><ul>
      <li class="t4">Connaître les étapes du développement du Web</li>
      <li class="t4">Reconnaître les pages sécurisées</li>
      <li class="t4">Décomposer l’URL d’une page</li>
    </ul></td>
  </tr>
  <tr><td>8</td><td>1</td>
    <td><ul>
      <li class="t4">Décomposer le contenu d’une requête HTTP et identifier les paramètres passés</li>
      <li class="t4">Étudier et modifier une page HTML simple</li>
      <li class="t4">Maîtriser les renvois d’un texte à différents contenus</li>
      <li class="t4">Inspecter le code d’une page hébergée par un serveur et distinguer ce qui est exécuté par le client et par le serveur</li>
    </ul></td>
  </tr>
  <tr><td colspan="3" class="vac"><b>Vacances de Février</b></td></tr>
  <tr><td>12</td><td>-</td>
    <td rowspan="2" class="align-middle">Journées banalisées (contrôles trimestriels)</td>
  </tr>
  <tr><td>13</td><td>-</td>
  </tr>
  <tr><td>12</td><td>2</td>
    <td><ul>
      <li class="t4">Étudier et modifier une page HTML simple</li>
      <li class="t4">Maîtriser les renvois d’un texte à différents contenus</li>
      <li class="t4">Inspecter le code d’une page hébergée par un serveur et distinguer ce qui est exécuté par le client et par le serveur</li>
    </ul></td>
  </tr>
  <tr><td>13</td><td>3</td>
  <td><ul>
      <li class="t4">Connaître certaines notions juridiques (licence, droit d’auteur, droit d’usage, valeur d’un bien)</li>
      <li class="t4">Mener une analyse critique des résultats fournis par un moteur de recherche</li>
      <li class="t4">Comprendre les enjeux de la publication d’informations</li>
      <li class="t4">Maîtriser les réglages les plus importants concernant la gestion des cookies, la sécurité et la confidentialité d’un navigateur</li>
      <li class="t4">Sécuriser sa navigation en ligne et analyser les pages et
fichiers</li>
    </ul></td>
  </tr>
  <tr><td class="modOdd" colspan="3"><div class="float-start"><b>[ Thème 5 : <span class="t5">Les réseaux sociaux</span> ]</b></div></td></tr>
  <tr><td>14</td><td>0</td>
    <td></td>
  </tr>
  <tr><td>15</td><td>1</td>
    <td></td>
  </tr>
  <tr><td>16</td><td>2</td>
    <td></td>
  </tr>
  <tr><td colspan="3" class="vac"><b>Vacances de Pâques</b></td></tr>
  <tr><td>19</td><td>3</td>
    <td></td>
  </tr>
  <tr><td class="modOdd" colspan="3"><div class="float-start"><b>[ Thème 6 : <span class="t6">Informatique embarquées et objets connectés</span> ]</b></div></td></tr>
  <tr><td>20</td><td>0</td>
    <td></td>
  </tr>
  <tr><td>21</td><td>0</td>
    <td></td>
  </tr>
  <tr><td>22</td><td>0</td>
    <td></td>
  </tr>
  <tr><td>23</td><td>0</td>
    <td></td>
  </tr>
  </tbody>
</table>

## État des lieux de la programmation

### Capacité non abordées ou partiellement

#### **Partager de fichiers, paramétrer des modes de synchronisation**

Le partage de fichier fait partie de la préparation d'une activité (animation de débat) dans la sequence "Les données structurées et leur traitement". Ce partage est effectué depuis la plateforme, neutre, "ÉcoleDirecte". Par contre, le paramétrage de mode de synchronisation n'est pas présent sur EcoleDirecte. 

Idéalement, il me faudrait utiliser des grandes plateformes telles que Google Drive, pCloud, DropBox, etc... Mais ceci présente plusieurs problèmes : 

- cela me sort de ma position neutre car je promouvois des outils appartenant à des entreprises privées. Une idée serait d'animer une activité où les élèves auraient le choix entre plusieurs plateformes de partage de fichier. Ce panel risque cependant de complexifier considérablement l'activité si elle est menée en classe. Le travail pourrait être fait à la maison, mais cela soulève d'autres problèmes (accès à un ordinateur, internet, le soucis de la protection des données toujours, ...)
- la protection des données personnelles sur ces plateformes n'est pas garantie, il faut alors créer des emails factices avec les élèves pour ne pas avoir à se confronter aux problèmes liés aux données personnelles. Cette étape va complexifiera d'avantage l'activité.

Ma réflexion s'est pour l'instant arrêtée à ce niveau.

#### **Identifier les principales causes de la consommation énergétique de données ainsi que leur ordre de grandeur**

Cette capacité est inscrite dans le programme dans le thème "Les données structurées et leur traitement" qui est un thème assez théorique par rapport aux autres du programme. J'ai préféré ne pas l'aborder à cet endroit du programme car je pense qu'elle sera d'avantage pertinente pour les élèves si nous l'abordons dans un thème "plus pratique".

Ainsi, cette capacité sera couverte soit dans le thème dédié au Web, soit dans le thème dédié aux réseaux sociaux.
