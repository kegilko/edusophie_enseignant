# Fiche de Séance nº6 (projet)

+++bulle matthieu
  le contenu de cette séance est similaire au contenu de la séance précédente
+++

+++bulle matthieu droite
  les ajouts ou modifications par rapport à la séance précédente sont <span class="sv">surlignées en vert</span>, les retraits sont <span class="so">barrés et surlignés en orange</span>
+++

<style>
#lessonBlocHTML-content table th{
    text-align:center;
}
#lessonBlocHTML-content p,#lessonBlocHTML-content li { 
  text-align: justify;
  text-align-last: left;
}
#lessonBlocHTML-content .sv{
  background-color: #96ff96; 
}
#lessonBlocHTML-content .so{
  background-color: #ffe596;
  text-decoration:line-through;
}


div.text-center:nth-child(13) > button:nth-child(1){
  background-color: #96ff96 !important;
}

div.text-center:nth-child(16) > button:nth-child(1){
  background-color: #96ff96 !important;
}



</style>

<table class="table table-sm">
    <thead>
        <tr><td class="align-middle text-center" colspan="100"><b>Objectif(s) de la séance</b></td></tr>
    </thead>    
    <tbody>
        <tr><td><ul class="mb-0">
            <li>Continuer les projets avec les élèves</li>
            <li>Laisser travailler davantage les élèves en autonomie en les incitant à s'entre aider entre groupes</li>
            <li>Observer les répartitions des rôles et du travail (donner des consignes si besoin)</li>
            <li class="sv">À l'issue de cette séance, tous les groupes doivent au moins avoir réussi à développer les fonctionnalités obligatoires du projet</li>
        </ul></td></tr>
    </tbody>
</table>

<table class="table table-sm text-nowrap">
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Durée de la séance</b></td>
            <td>55mn (inspection) + 55mn</td>
        </tr>
    </tbody>
</table>

<table class="table table-sm">
    <thead>
        <tr>
            <th>Capacité(s) visée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr><td>
          <p>Les capacités listées ci-dessous sont les principales capacités couvertes à travers le projet. Cette liste n'est donc pas propre à la séance en cours.</p>
          
          <p class="mb-0"><b>Thème :</b> Traitement de données en tables</p>
          <ul class="mb-0">
            <li>Importer une table depuis un fichier texte tabulé ou un fichier CSV</li>
            <li>Trier une table suivant une colonne</li>
          </ul>
          <p class="mb-0"><b>Thème :</b> Représentation des données : types construits</p>
          <ul class="mb-0">
            <li>Lire et modifier les éléments d’un tableau grâce à leurs index</li>
            <li>Utiliser des tableaux de tableaux pour représenter des matrices : notation a [i] [j]</li>
            <li>Itérer sur les éléments d’un tableau</li>
          </ul>
          <p class="mb-0"><b>Thème :</b> Interactions entre l’homme et la machine sur le Web</p>
          <ul class="mb-0">
            <li>Identifier les différents composants graphiques permettant d’interagir avec une application Web</li>
            <li>Analyser et modifier les méthodes exécutées lors d’un clic sur un bouton d’une page Web</li>
            <li>Distinguer ce qui est exécuté sur le client ou sur le serveur et dans quel ordre</li>
            <li>Distinguer ce qui est mémorisé dans le client et retransmis au serveur</li>
          </ul>
          <p class="mb-0"><b>Thème :</b> Langages et programmation</p>
          <ul class="mb-0">
            <li>Mettre en évidence un corpus de constructions élémentaires</li>
            <li>Prototyper une fonction</li>
            <li>Utiliser la documentation d’une bibliothèque</li>
          </ul>
        </td></tr>
    </tbody>
</table>

<table class="table table-sm">
    <thead>
        <tr>
            <th>Compétence(s) travaillée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">
              <p>Les compétences listées ci-dessous sont les principales compétences couvertes à travers le projet. Cette liste n'est donc pas propre à la séance en cours.</p>      
              <p class="mb-0"><b>Compétences de discipline :</b></p>
              <ul class="mb-0">
                  <li>Analyser et modéliser un problème en termes de flux et de traitement d’informations</li>
                  <li>Décomposer un problème en sous-problèmes, reconnaître des situations déjà analysées et réutiliser des solutions</li>
                  <li>Comprendre et réutiliser des codes sources existants</li>
                  <li>Mobiliser les concepts et les technologies utiles pour assurer les fonctions d’acquisition, de mémorisation, de traitement et de diffusion des informations</li>
                  <li>Développer des capacités d’abstraction et de généralisation</li>
              </ul>
              <p class="mb-0 mt-2"><b>Compétences transversales :</b></p>
              <ul class="mb-0">
                  <li>Faire preuve d’autonomie, d’initiative et de créativité</li>
                  <li>Présenter un problème ou sa solution</li>
                  <li>Coopérer au sein d’une équipe</li>
                  <li>Rechercher de l’information, apprendre à utiliser des sources de qualité, partager des ressources</li>
              </ul>
            </td>
        </tr>
    </tbody>
</table>

<table class="table table-sm">
    <thead>
        <tr><td class="align-middle text-center" style="width:0" colspan="100"><b>Environnement à préparer</b></td></tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Aménagement de l'espace</b></td>
            <td><ul class="mb-0">
              <li>Par défaut, la salle dispose les élèves en îlots</li>
              <li>Les élèves se regroupent dans la salles en fonction de la composition de leur groupe. Pendant les vacances de février, chaque élève a été invité à se choisir un binôme et à me signaler sur quel projet il souhaite travailler. J'ai ensuite rassemblé les binomes pour former des groupes équilibrés</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Matériel</b></td>
            <td><ul class="mb-0">
                <li>Enseignant : tableau</li>
                <li>Élèves : un ordinateur par élève + tableau + clé USB</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Software</b></td>
            <td><ul class="mb-0">
                <li>Élèves : thonny + un éditeur de texte pour les codes HTML et CSS</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Support(s) utilisé(s)</b></td>
            <td><ul class="mb-0">
                <li><i class="isFile fas fa-file-alt"></i> +++lien "/enseignant/didactique/modeles/projet/grille-_evaluation-_projet.md" "<b>[grille d'évaluation de projet]</b>" : un exemplaire est fourni à chaque groupe, la version numérique est disponible en ligne et a été envoyé par mail pendant les vacances de février</li>
                <li><i class="isFile fas fa-file-alt"></i> +++lien "/apprenant/01-__-_NSI-_premiere/08-__-_representation-_des-_entiers-_relatifs-_et-_reels-_en-_binaire-_et-_tri-_et-_fusion-_de-_donnees-_dans-_une-_table/projets/site-_ecommerce/cahier-_des-_charges.md" "<b>[cahier des charges]</b> projet site ecommerce" : la version numérique est disponible en ligne et a été envoyée par mail pendant les vacances de février. Une base de code est disponible dans le cahier des charges</li>
                <li><i class="isFile fas fa-file-alt"></i> +++lien "/apprenant/01-__-_NSI-_premiere/08-__-_representation-_des-_entiers-_relatifs-_et-_reels-_en-_binaire-_et-_tri-_et-_fusion-_de-_donnees-_dans-_une-_table/projets/evaluateur-_de-_mot-_de-_passe/cahier-_des-_charges.md" "<b>[cahier des charges]</b> projet évaluateur de mot de passe" : un exemplaire est fourni à chaque groupe, la version numérique a été envoyée par mail pendant les vacances de février. Une base de code est disponible dans le cahier des charges</li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Déroulement prévu de la séance

<table class="table table-sm">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#0</b></td>
            <td colspan="3">Daily meeting</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap sv" style="width:0">00h00 -> 00h15</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>Chaque élève ou binôme devra, durant le daily meeting : <ul>
                  <li>Parler du travail qu'il a réalisé la séance d'avant</li>
                  <li>Parler des difficultés qui ont pu être rencontrées</li>
                  <li>Exposer ce sur quoi il va travailler aujourd'hui et ce qu'il espère réaliser</li>
                </ul>
              </li>
              <li>À travers le daily meeting, un point est fait sur les observations faîtes lors de la précédente séance. Des remarques générales peuvent être émises et également être à destination d'un groupe spécifique</li>
              <li class="so">Une nouvelle consigne est présentée : j'interviens, auprès d'un groupe demandant de l'aide, un nombre de fois limité pendant la séance. Le but est de pousser les élèves à devenir plus autonome</li>
              <li class="sv">Il est rappelé qu'il s'agit de la dernière séance du projet et que le temps de travail de fusion frontend / backend n'est pas à négliger</li>
              <li>Un temps est pris pour échanger avec les élèves pour toute question</li>
            </ol></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#1</b></td>
            <td colspan="3">Phase "projet"</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap sv" style="width:0">00h15 -> 00h55</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li value="0">Accompagnement limité : chaque groupe peut me solliciter au plus 2 deux fois pendant la séance. Je m'autorise à intervenir de ma propre initiative auprès d'un groupe ou auprès d'un élève si j'estime que c'est important</li>
              <li>Je détecte et aide les élèves en difficultés et/ou isolés dans les groupes</li>
              <li>Je répond aux appels à l'aide et j'observe le travail des groupes. Des remarques peuvent être faîtes sur l'organisation intra-groupe (communication, gestion du travail...), des consignes peuvent être données</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Élèves : grille d'évaluation et cahier des charges</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : tableau (si il y a besoin de ré-expliquer un concept à travers un schéma)</li>
                <li>Élèves : ordinateur portable et tableau</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Le groupe rencontre une difficulté sur un choix à faire : je propose aux élèves de trancher à l'aide d'un "pierre/feuille/ciseaux"</li>
                <li>Un groupe est incomplet dû à l'absence d'un élève : j'annonce au groupe qu'il bénéficiera d'une petite aide supplémentaire de ma part</li>
                <li>Un élève reste passif : je l'invite à échanger de place avec un de ses camarades et/ou de se lever et de voir les travaux des autres groupes</li>
                <li class="sv">La plupart des groupes n'ont pas terminé de développer les fonctionnalités principales du projet : étant donné que nous terminons le programme vers mi-fin Avril, il serait possible d'ajouter une dernière séance pour le projet</li>
            </ul></td>
        </tr>
    </tbody>
</table>

Afin d'avoir un temps équilibré entre mes deux groupes de première, il restera 55mn pour les élèves de première G1 pour leurs projet. Ces 55mn seront intégrées dans la séance suivante. Voici ci-dessous ce que j'ai initialement prévu pour ces 55 dernières minutes : 

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#2</b></td>
            <td colspan="3">Préparation de l'oral de présentation de projet</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap sv" style="width:0">00h55 -> 01h50</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li class="sv">J'invite les élèves à fusionner leurs différents travaux ensemble et à tester si l'application fonctionne</li>
              <li>J'invite les élèves à enregistrer leur travail dans une clé usb<span class="sv"> avant la fin de l'heure afin de pouvoir poursuivre le travail en dehors du cours si besoin<span> <span class="so">au cas où (absence éventuelle d'un élève dans le groupe ou de l'enseignant lors de la prochaine séance)</span></span></span></li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Élèves : clé USB</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li class="sv">Le travail final de fusion peut poser diverses difficultés. Je m'assurerai que les groupes puissent mener cette étape avant la fin du temps imparti. Je sensibiliserai les élèves (en complément de la grille d'évaluation dont ils disposent) sur la qualité du rendu de travail : rendu sous forme d'archive, fichiers biens structurés, présence d'une documentation, ...</li>
            </ul></td>
        </tr>
    </tbody>
</table>