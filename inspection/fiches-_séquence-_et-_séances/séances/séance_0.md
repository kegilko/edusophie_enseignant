# Fiche de Séance nº0

<style>
#lessonBlocHTML-content table th{
    text-align:center;
}
#lessonBlocHTML-content p,li { 
  text-align: justify;
  text-align-last: left;
}
</style>

<table class="table table-sm">
    <thead>
        <tr><td class="align-middle text-center" colspan="100"><b>Objectif(s) de la séance</b></td></tr>
    </thead>    
    <tbody>
        <tr><td><ul class="mb-0">
            <li>Réinvestir certaines notions travaillées dans des séances antérieures</li>
            <li>S'appuyer sur ces réinvestissements pour introduire les deux sujets : <ul>
              <li>Représenter un nombre binaire négatif</li>
              <li>Calculer des approximations de nombre binaires réels</li>
            </ul></li>
        </ul></td></tr>
    </tbody>
</table>

<table class="table table-sm text-nowrap">
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Durée de la séance</b></td>
            <td>1h50</td>
        </tr>
    </tbody>
</table>

<table class="table table-sm">
    <thead>
        <tr>
            <th>Capacité(s) visée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr><td>
          <p class="mb-0"><b>Thème :</b> représentation des données: types et valeurs de base</p>
          <ul class="mb-0">
            <li>(<b>Pré-req</b>) Passer de la représentation d’une base dans une autre</li>
            <li>(<b>Pré-req</b>) Évaluer le nombre de bits nécessaires à l’écriture en base 2 d’un entier, de la somme ou du produit de deux nombres entier</li>
            <li>Utiliser le complément à 2</li>
            <li>Calculer sur quelques exemples la représentation de nombres réels : 0.1, 0.25 ou 1/3</li>
          </ul>
        </td></tr>
    </tbody>
</table>

<table class="table table-sm">
    <thead>
        <tr>
            <th>Compétence(s) travaillée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">
              <p class="mb-0"><b>Compétences de discipline :</b></p>
              <ul class="mb-0">
                  <li>Reconnaître des situations déjà analysées et réutiliser des solutions</li>
                  <li>Mobiliser les concepts pour assurer les fonctions de mémorisation et de traitement des informations</li>
                  <li>Développer des capacités d’abstraction et de généralisation</li>
              </ul>
              <p class="mb-0 mt-2"><b>Compétences transversales :</b></p>
              <ul class="mb-0">
                  <li>Faire preuve d’autonomie</li>
                  <li>Présenter un problème et sa solution</li>
              </ul>
            </td>
        </tr>
    </tbody>
</table>

<table class="table table-sm">
    <thead>
        <tr><td class="align-middle text-center" style="width:0" colspan="100"><b>Environnement à préparer</b></td></tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Aménagement de l'espace</b></td>
            <td><ul class="mb-0">
              <li>Par défaut, la salle dispose les élèves en îlots</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Matériel</b></td>
            <td><ul class="mb-0">
                <li>Enseignant : ordinateur + video-projecteur</li>
                <li>Élèves : un ordinateur par élève</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Support(s) utilisé(s)</b></td>
            <td><ul class="mb-0">
                <li>Enseignant & Élèves : <ul>
                  <li><i class="isFile fas fa-file-alt"></i> +++lien "/apprenant/01-__-_NSI-_premiere/08-__-_representation-_des-_entiers-_relatifs-_et-_reels-_en-_binaire-_et-_tri-_et-_fusion-_de-_donnees-_dans-_une-_table/cours/00-__-_representation-_des-_entiers-_relatifs-_et-_reels-_en-_binaire.md" "<b>[cours & activité(s)]</b> Représentation des entiers relatifs et réels" (disponibles en ligne)</li>
                </ul></li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Déroulement prévu de la séance

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#0</b></td>
            <td colspan="3"><i class="isFile fas fa-file-alt"></i> <a href="/apprenant/01-__-_NSI-_premiere/08-__-_representation-_des-_entiers-_relatifs-_et-_reels-_en-_binaire-_et-_tri-_et-_fusion-_de-_donnees-_dans-_une-_table/cours/00-__-_representation-_des-_entiers-_relatifs-_et-_reels-_en-_binaire.md" target="_blank">Introduction de la séance</a></td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h00 -> 00h15</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>
            <td colspan="10">
            <ol class="mb-0" start="0">
              <li>Présentation des objectifs de la séance</li>
              <li>Rappel de ce que nous avons appris dans une séance antérieure portant sur le thème "représentation des données: types et valeurs de base" (+ rappel des enjeux du thème). Un élève rappellera au tableau comment convertir un nombre en base 10 à un nombre en base 2</li>
              <li>Introduction de la première problématique soulevée dans la séance : comment un ordinateur représente un nombre binaire ?</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : cours projeté au tableau</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : video projecteur + ordinateur portable</li>
                <li>Élèves : tableau</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Pour l'intervention au tableau, privilégier un élève qui a des difficultés avec les conversion base 10 => base 2. Son intervention sera utile pour d'autres éventuels élèves présentant aussi des difficultés</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#1</b></td>
            <td colspan="3">Étude d'un cas naïf de représentation d'un nombre binaire négatif</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h15 -> 00h25</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>Les élèves sont invités à réfléchir sur une manière de représenter un nombre binaire négatif en utilisant seulement des 0 et des 1</li>
              <li>Nous discutons des différentes approches, je creuse avec eux l'approche consistant à ajouter un bit représentant le signe du nombre</li>
              <li>Je présente les deux défauts de cette approche (0 possédant deux représentations possible + problème de calcul). Des élèves interviennent au tableau pour effectuer conversions et calculs afin de mettre en évidence le deuxième défaut dont j'ai parlé</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : cours projeté au tableau</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : video projecteur + ordinateur portable</li>
                <li>Élèves : tableau</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>De même que pour l'étape précédente, je privilégie les élèves qui ont des difficultés avec les conversion base 10 => base 2 pour l'intervention au tableau.</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#2</b></td>
            <td colspan="3"><i class="isFile fas fa-file-alt"></i> <a href="/apprenant/01-__-_NSI-_premiere/08-__-_representation-_des-_entiers-_relatifs-_et-_reels-_en-_binaire-_et-_tri-_et-_fusion-_de-_donnees-_dans-_une-_table/activites/applications-_directes/representation-_d--__un-_binaire-_relatif-_--_-_methode-_du-_complement-_a-_2.md" target="_blank">Activité d'introduction et d'entraînement sur le complément à 2</a></td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h25 -> 01h00</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>Les élèves démarrent en autonomie cette première activité de séance. Le but de cette activité et de leur présenter la méthode du complément à 2 et de s'entraîner à l'utiliser</li>
              <li>Les élèves doivent noter les réponses ( +++emoji-write ) dans leur cours</li>
              <li>Mes interventions varient en fonction des avancées individuelles et collectives de la classe</li>
              <li>Je provoque des phases de corrections intermédiaires tout au long de cette étape où les élèves sont invités à inscrire leurs réponses au tableau</li>
              <li>Des questions sont prévues pour les élèves qui avancent plus vite</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : activité et corrections projetées au tableau</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : video projecteur + ordinateur portable</li>
                <li>Élèves : cours + tableau</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Je privilégie les élèves qui ont des difficultés avec les conversion base 10 => base 2 pour l'intervention au tableau</li>
                <li>En fonction des avancées individuelles, je peux solliciter un élève en avance pour venir épauler un élève en difficulté</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#3</b></td>
            <td colspan="3">Fin de la première partie de la séance : récapitulatif et échanges avec les élèves</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">01h00 -> 01h05</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>Nous revenons au niveau du cours, un élève est sollicité pour lire à voix haute la partie "Résumons" du cours</li>
              <li>Pendant la lecture du bloc résumons, je peux intervenir sur des points qui me semblent important et/ou sur des erreurs récurrentes que j'ai pu observer durant l'activité</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : cours projeté au tableau</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : video projecteur + ordinateur portable</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#4</b></td>
            <td colspan="3">Début de la deuxième partie de la séance</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">01h05 -> 01h15</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>Je présente aux élèves un exemple de calcul qu'un ordinateur semble incapable de faire avec précision (addition entre deux nombres flottants)</li>
              <li>Les élèves sont invités à tester d'autres calculs via l'éditeur python intégré au cours</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : cours projeté au tableau</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : video projecteur + ordinateur portable</li>
                <li>Élèves : ordinateur</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#5</b></td>
            <td colspan="3"><i class="isFile fas fa-file-alt"></i> <a href="/apprenant/01-__-_NSI-_premiere/08-__-_representation-_des-_entiers-_relatifs-_et-_reels-_en-_binaire-_et-_tri-_et-_fusion-_de-_donnees-_dans-_une-_table/activites/applications-_directes/representation-_de-_nombres-_reels-_en-_binaire.md" target="_blank">Activité dédiée à la représentation de nombres réels en binaire</a></td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">01h15 -> 01h50</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>Les élèves démarrent en autonomie cette seconde activité de séance. Le but de cette activité et de leur présenter une méthode pour convertir un nombre à virgule base 10 en binaire, et vice et versa</li>
              <li>Les élèves doivent noter les réponses ( +++emoji-write ) dans leur cours</li>
              <li>Mes interventions varient en fonction des avancées individuelles et collectives de la classe</li>
              <li>Je provoque des phases de corrections intermédiaires tout au long de cette étape où les élèves sont invités à inscrire leurs réponses au tableau</li>
              <li>Des questions sont prévues pour les élèves qui avancent plus vite</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : activité et corrections projetées au tableau</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : video projecteur + ordinateur portable</li>
                <li>Élèves : cours + tableau</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>La partie 'obligatoire' contenue dans l'activité devrait se faire en beaucoup moins de temps que les 35 minutes estimées. Cette amplitude me permettra de détecter les élèves en difficulté et de prendre le temps pour intervenir auprès d'eux pour les aider</li>
                <li>En fonction des avancées individuelles, je peux solliciter un élève en avance pour venir épauler un élève en difficulté</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#6</b></td>
            <td colspan="3">Fin de la séance : récapitulatif et échanges avec les élèves</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap text-red" style="width:0">01h50 -> 02h00</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>
            <td colspan="10">
            <p class="text-red">Il est possible que cette partie ne puisse avoir lieu en classe par manque de temps. Les élèves seront alors invité à la suivre chez eux</p>
            <ol class="mb-0" start="0">
              <li>Nous revenons au niveau du cours, un élève est sollicité pour lire à voix haute la partie "Résumons" du cours</li>
              <li>Pendant la lecture du bloc résumons, je peux intervenir sur des points qui me semblent importants et/ou sur des erreurs récurrentes que j'ai pu observer durant l'activité</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : cours projeté au tableau</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : video projecteur + ordinateur portable</li>
            </ul></td>
        </tr>
    </tbody>
</table>
