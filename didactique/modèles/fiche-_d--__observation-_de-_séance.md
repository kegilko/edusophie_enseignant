# fiche d'observation de séance

<table class="table table-sm text-nowrap mt-5">
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Observateur</b></td>
            <td class="p-0 position-relative">{{inputText-td "matthieu lopez"}}</td>
            <td class="text-center" style="width:0"><b>Enseignant(e) de la séance</b></td>
            <td class="p-0 position-relative">{{inputText-td "M./Mme. xxx"}}</td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Date de la séance</b></td>
            <td class="p-0 position-relative">{{inputText-td "jeudi 6 Janvier 2022"}}</td>
            <td class="text-center" style="width:0"><b>Niveau de la classe</b></td>
            <td class="p-0 position-relative">{{inputText-td "Terminale NSI"}}</td>
        </tr>
    </tbody>
</table>

+++nobreak

## Contexte de la séance

{{textarea}}

nobreak+++

+++nobreak

## Aménagement de l'espace et équipement utilisé

{{textarea}}

nobreak+++

+++nobreak

## Observation chronologique

{{textarea}}

nobreak+++

+++nobreak

## Analyse

{{textarea}}

nobreak+++