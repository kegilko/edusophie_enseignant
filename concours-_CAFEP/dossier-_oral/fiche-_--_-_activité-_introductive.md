# Bases de données : activité introductive

<div class="p-3 mb-2 bg-danger text-white d-print-none">Attention ! les <span class="text-blue">informations bleutées</span> sont à destination de l'enseignant. Ces informations ne seront pas présentes si ce document est imprimé.</div>

+++bulle matthieu
    Aujourd'hui nous démarrons un tout nouveau chapitre dédié à un domaine majeur de l'inormatique, les <b>Bases de données</b> ! Avant de s'attaquer au vif du sujet, nous l'introduisons à travers une courte activité
+++

## Une histoire de données

<p class="text-center">Mardi 9 février 2021, le directeur général du site leboncoin a posté le tweet suivant</p>

![https://twitter.com/ajouteau/status/1359080640122093572?ref_src=twsrc%5Etfw](https://i.ibb.co/vJG49DV/Screenshot-from-2021-04-18-06-38-57.png)

Supposons que le site soit consulté chaque jours en moyenne par 10 millions d'utilisateurs uniques ( ~ 20,4 millions / 2 ). Supposons également qu'une visite sur 50 aboutit à une commande sur le site.

**1) Combien d'utilisateurs totaux auront visité le site en 30 jours ?**

<div class="border border-dark mb-3 p-1 pb-3">
    <span class="d-print-hidden text-blue">Nous supposons que 10 millions d'utilsateurs consultent le site quotidiennement, donc 300 000 000 ( 30 * 10 000 000 ) visiteurs auront visité le site leboncoin en 30 jours
    </span>
</div>

**2) Combien y aura-t'il eu de commandes après 30 jours ?**

<div class="border border-dark mb-3 p-1 pb-3">
    <span class="d-print-hidden text-blue">Nous supposons qu'une visite sur 50 aboutit à une commande sur le site, donc 6 000 000 ( 300 000 000 / 50 ) commandes auront été effectuées en l'espace 30 jours
    </span>
</div>

## Petits rappels : traiter des données en table

Supposons que le site leboncoin stocke les informations de toutes les commandes de tous les utilisateurs dans un fichier '<b>lbc_achats.csv</b>'.

**3) Rappeler ce qu'est un fichier .csv et donner un exemple de contenu d'un tel fichier**

<div class="border border-dark mb-3 p-1 pb-3">
    <span class="d-print-hidden text-blue">
        Un fichier .csv est un fichier contenant du texte, et ce texte suit une structure standardisée facilitant la conservation et le traitement ultérieur de données. CSV est l'acronyme de <b>C</b>omma-<b>S</b>eparated <b>V</b>alues, mais il est tout à fait possible d'utiliser un autre séparateur tel que le point virgule, un exemple : <br><br>
        jeu;difficulté;score maximal<br>
        pacman;medium;190000<br>
        arkanoid;hard;280000
    </span>
</div>

En général, lorsqu'un utilisateur X effectue une commande sur un site de vente en ligne, ce dernier consulte la page "mes commandes" pour vérifier que sa dernière commande a bien été prise en compte. Le serveur gérant le site leboncoin doit alors ouvrir son fichier '<b>lbc_achats.csv</b>' pour en extraire les informations de toutes les commandes de l'utilisateur X. Nous allons simuler cette opération serveur pour l'utilisateur Moreau Romain.

+++bulle matthieu 75
    La suite se fait sur ordinateur ! demandez à votre enseignent de vous transmettre 'la clé usb'
+++

+++bulle matthieu droite 75
    Copiez/collez le dossier 'introduction_bdd' sur votre ordinateur et passez votre clé à un autre groupe qui n'a pas encore copié le dossier. 
+++

Une fois le dossier copié sur votre ordinateur, vous remarquez qu'il contient un ensemble de fichiers : 

- '**lbc_commandes.csv**' : contient les informations des commandes effectuées en 30 jours (6 000 000 en tout)
- '**mini\_lbc\_commandes.csv**' : contient les 10 premières entrées du fichier 'lbc_commandes.csv'
- '**liste\_des\_commandes.py**' : un script python ayant pour but de lister les informations des commandes de l'utilisateur X

+++bulle matthieu 75
    Attention ! pour l'instant n'utilisez pas le fichier '<b>lbc_achats.csv</b>' qui est assez volumineux. 
+++

**4) Complétez le script 'liste\_des\_commandes.py' afin qu'il affiche le resultat de sa recherche directement dans le terminal. Puis exécutez le avec en paramètres le fichier .csv '`mini_lbc_achats.csv`' et `Moreau` et `Romain`**

Une fois fonctionnel, votre script '<b>liste\_des\_commandes.py</b>' avec les paramètres ci-dessus devrait vous afficher un résultat ressemblant à ceci : 

```md
Recherche des commandes de Moreau Romain...
resultat(s) :
jour 01;Moreau;Romain;93922733604317328639;19
jour 01;Moreau;Romain;00076732160050035072;25

--- temps d'execution du script : X.X seconde(s) ---
```

## Arriver aux limites de notre approche

Au début de cette activité, nous avons estimé qu'au bout de 30 jours, 6 millions de commandes seraient passées. Le fichier 'lbc\_achats.csv' contient 6 millions de commandes factices et maintenant que notre script de recherche est prêt, nous allons pouvoir tester une recherche sur un fichier volumineux. 

**5) Utilisez le script 'liste\_des\_commandes.py' avec en paramètres le fichier .csv '`lbc_achats.csv`' et `Moreau` et `Romain`.**

+++bulle matthieu 75
    Attention ! le traitement peut être long étant donnée le nombre d'entrées dans le fichier 'lbc_achats.csv'.
+++
+++bulle matthieu droite 75
    Avant exécution du script, je vous conseil vivement de fermer toutes les applications gourmandes sur l'ordinateur (comme le navigateur web par exemple)
+++


**6) Notez le temps d'exécution obtenu (en secondes) :** \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ <span class="d-print-none text-blue">(9,44 secondes)</span>

<span class="text-blue d-print-none">Bien entendu, le temps varie d'un ordinateur à l'autre, d'une exécution à l'autre... mais le but ici est avant tout de relever un "ordre de grandeur".</span>

Nous avons maintenant une estimation du temps que mettrait le serveur pour rassembler les informations liées aux commandes d'un utilisateur donné.

**7) Qu'en conclure sur l'expérience qu'aura notre utilisateur depuis son navigateur lorsqu'il accèdera à la page listant toutes ses commandes ?**

<div class="border border-dark mb-3 p-1 pb-3">
    <span class="d-print-hidden text-blue">Notre utilisateur devra attendre au moins 9,44 secondes avant que son navigateur affiche les données de la page "mes commandes". Cette longue attente impactera de manière très négative l'expérience utilisateur.</span>
</div>


+++bulle matthieu 75
    Pour les groupes arrivées ici, je vous invite à venir en aide aux autres !
+++

<span class="text-blue d-print-none">Les questions suivantes ne seront pas rédigées sur le document imprimé à l'élève. Les groupes les plus avancés pourront réfléchir à comment aller au delà des calculs actuels et aider les groupes en retard. Ce temps permets aux groupes nécessaitant d'avantage de temps  d'avancer à leur rythme.</span>

## Pulvériser les limites de notre approche

<b>8) <span class="d-print-hidden text-blue">Étant donné que le site leboncoin existe depuis une quinzaine d'année. Combien de temps faudrait il pour rechercher les commandes d'un utilisateur dans un fichier .csv cumulant les commandes depuis 10 ans ? Pour simplifier les calculs, nous supposerons que tous les mois sont composés de 30 jours.</span></b>

<div class="border border-dark mb-3 p-1 pb-3">
    <span class="d-print-hidden text-blue">Pour 30 jours de données, nous avons déterminé qu'il faudrait environ 9,44 secondes pour rechercher les informations des commandes d'un utilisateur. Pour un .csv construit depuis 15 années, il faudra environ 18 minutes et 52 secondes ( 10 années * 12 mois * 9,44 sec = 1132,8 sec ) pour trouver les commandes d'un utilisateur</span>
</div>

<b>9) <span class="d-print-hidden text-blue">Le site a reçu 20.4 millions de visites le 09/02/2021. Nous avons supposé qu'un visiteur sur 50 effectue un achat et, donc, se rendra sur la page listant ses commandes. Calculer en secondes puis en années (de 360 jours) le temps qu'un serveur monocore devra prendre pour chercher toutes les informations des commandes de tous les utilisateurs qui les demandent.</span></b>

<div class="border border-dark mb-3 p-1 pb-3">
    <span class="d-print-hidden text-blue">Pour une demande, nous avons vu qu'il faut environ 1132,8 sec au serveur pour la traiter. En cette journée du 09/02/2021, et supposant qu'un utilisateur sur 50 consulte la page "mes commandes", le serveur aura besoin d'environ 4,6218 * 10^8 secondes ( 1132,8 sec * 20 400 000 utilisateurs / 50 ) soit presque 15 années de 360 jours ( [4,6218 * 10^8] / [ 360 jours * 24 heures * 3600 secondes ] )  pour traiter les requêtes de la journée des utilisateurs.</span>
</div>

<b>10) <span class="d-print-hidden text-blue">Quelle(s) conclusion(s) en tirer ?</span></b>

<div class="border border-dark mb-3 p-1 pb-3">
    <span class="d-print-hidden text-blue">La recherche d'information dans un fichier texte est inefficace lorsque ce dernier devient volumineux. Nous avons mis en lumière ce problème à travers un contexte web, mais ce problème s'étends bien entendu bien au delà :
        <ul>
            <li>Comment stocker, mettre à jours et récupérer efficacement toutes les informations liées aux livres mis à disposition par l'ensemble des bibliothèques de France ?</li>
            <li>Comment stocker, mettre à jours et récupérer efficacement toutes les informations qu'échangent en temps réel des milliers voir des millions de joueurs sur un jeu en ligne ?</li>
            <li>Comment stocker, mettre à jours et récupérer efficacement toutes les informations liées au génome humain ? et aux autres animaux ?</li>
            <li>etc ...</li>
        </ul>
    
    </span>
</div>