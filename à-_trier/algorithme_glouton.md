# algorithme_glouton

## problème du voyageur

widget python
import sys

def voyageur(numeros_villes, matrice_distances, numero_ville_depart):

    numero_ville_courante = numero_ville_depart
    villes_visitees = [False for i in range(len(numeros_villes))]
    villes_visitees[numero_ville_depart] = True

    parcours_total_km = 0
    parcours_total_numeros_villes = [numero_ville_depart]

    for i in range(len(numeros_villes) - 1):
        
        distance_ville_suivante = sys.maxsize
        numero_ville_suivante = None
        
        for numero_ville_candidate in range(len(numeros_villes)):
            if(numero_ville_courante != numero_ville_candidate and villes_visitees[numero_ville_candidate] != True and distance_ville_suivante > matrice_distances[numero_ville_courante][numero_ville_candidate]):
                distance_ville_suivante = matrice_distances[numero_ville_courante][numero_ville_candidate]
                numero_ville_suivante = numero_ville_candidate

        parcours_total_numeros_villes.append(numero_ville_suivante)
        parcours_total_km = parcours_total_km + distance_ville_suivante

        villes_visitees[numero_ville_suivante] = True
        numero_ville_courante = numero_ville_suivante

    parcours_total_km = parcours_total_km + matrice_distances[numero_ville_depart][numero_ville_courante]
    parcours_total_numeros_villes.append(numero_ville_depart)


    return (str(parcours_total_km), parcours_total_numeros_villes)
        
        
matrice_distances = [
    [None   ,55    ,303   ,188   ,183],
    [55     ,None  ,306   ,176   ,203],
    [303    ,306   ,None  ,142   ,153],
    [188    ,176   ,142   ,None  ,123],
    [183    ,203   ,153   ,123   ,None]
]

numeros_villes = [0,1,2,3,4]

numero_ville_depart = 0

rapport = voyageur(numeros_villes, matrice_distances, numero_ville_depart)

print(f"""Je pars de la ville numéro : {numero_ville_depart}
Voici mon parcours : {rapport[1]}
Ce qui donne au total {rapport[0]} km !
""")
    
widget

## Rendu de monnaie

widget python
euros = [1,2,5,10,20,50,100,200]

def monnaie(s):
    rendu = []
    i = len(euros) - 1
    
    while(s > 0):
        if (s >= euros[i]):
            s -= euros[i]
            rendu.append(euros[i])
        else:
            i -= 1
        
    return rendu
    
somme = 48
print(f"Avec {somme} euro(s), je te rends comme monnaie : {monnaie(somme)}")
widget

