# séance 1

## Objectifs 

<ul>
    <li>Rassembler les résultats des recherches des élèves sur les facteurs liés à la qualité de l'air</li>
    <li>Prendre connaissance du projet 'météo'</li>
    <li>Depuis un logiciel de gestion pour SGBD (<a href="https://sqlitebrowser.org/dl" target="_blank">SQLite Browser</a>)</li>
    <ul>
        <li>Créer une base de données</li>
        <li>Créer une table</li>
        <li>Créer des champs</li>
        <li></li>
    </ul>
</ul>

## Déroulement de la séance

Les estimations de temps moyen nécessaires sont pour moi pour l'organisation des 2 heures de la séance.

<table class="table col-10 mx-auto">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Étapes de la séance</th>
      <th scope="col">Estimation de la durée</th>
    </tr>
  </thead>
  <tbody>
    <tr>
        <th scope="row">1</th>
        <td>
            <p>En amont, les élèves ont du faire des recherches sur les critères qui déterminent la qualité de l'air.</p>
            <p>Trois élèves seront tirés au sort pour présenter rapidement le résultat des recherches. Cela servira à introduire le projet (dans ses grandes lignes) de 4 semaines à élaborer qui s'inscrit dans le thème "Bases de données"</p>
        </td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">2</th>
        <td>
            <p>On mets de côté le projet et on démarre la séance par une première activité.</p>
            <ul>
                <li>Utilisation d'un tableur pour manipuler des données</li>
                <li>Mises en évidence des limites du tableur (taille des données, recherche de données, ...)</li>
            </ul>
        </td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">3</th>
        <td>
            <p>Premier cours (magistral ?) de Bases de Données</p>
        </td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">4</th>
        <td>
            <p>Deuxième activité : </p><p>
            </p><ul>
                <li>Reprendre les données utilisées dans l'activité 1 et les stoker dans une BDD</li>
                <li>Utilisation du SGBD SQLite et du logiciel "SQLite Browser"</li>
            </ul>
        </td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">...</th>
        <td>~ à détailler ~</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">n</th>
        <td>
            <p>En fin de séance sera un peu plus détaillé le projet et sera donné comme consigne aux élèves de réfléchir à la composition de binômes. Je devrais connaitre la composition des binômes avant la fin de la séance 2.</p>
        </td>
        <td></td>
    </tr>
  </tbody>
</table>

## Les étapes de la séance en détail

### Étape 1

~ à détailler ~

### Étape 2

~ à détailler ~