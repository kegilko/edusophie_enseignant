# Fiche de Séance nº1

<style>
#lessonBlocHTML-content table th{
    text-align:center;
}
#lessonBlocHTML-content p,li { 
  text-align: justify;
  text-align-last: left;
}
</style>

<table class="table table-sm">


    <thead>


        <tr><td class="align-middle text-center" colspan="100"><b>Objectif(s) de la séance</b></td></tr>


    </thead>    


    <tbody>


        <tr><td><ul class="mb-0">


            <li>Réinvestir certaines notions travaillées dans des séances antérieures</li>


            <li>Présenter dans les grandes lignes le contenu du projet projet</li>


            <li>S'initier à la bibliothèque python pandas pour : <ul>


              <li>trier une table suivant une colonne</li>


              <li>fusionner des tables entre elles</li>


            </ul></li>


        </ul></td></tr>


    </tbody>


</table>



<table class="table table-sm text-nowrap">


    <tbody>


        <tr>


            <td class="text-center" style="width:0"><b>Durée de la séance</b></td>


            <td>1h50</td>


        </tr>


    </tbody>


</table>



<table class="table table-sm">


    <thead>


        <tr>


            <th>Capacité(s) visée(s)</th>


        </tr>


    </thead>


    <tbody>
        <tr><td>
          <p class="mb-0"><b>Thème :</b> Traitement de données en tables</p>
          <ul class="mb-0">
            <li>(<b>Pré-req</b>) Importer une table depuis un fichier texte tabulé ou un fichier CSV</li>
            <li>Trier une table suivant une colonne</li>
            <li>Construire une nouvelle table en combinant les données de deux tables</li>
          </ul>
        </td></tr>
    </tbody>
</table>



<table class="table table-sm">


    <thead>


        <tr>


            <th>Compétence(s) travaillée(s)</th>


        </tr>


    </thead>


    <tbody>


        <tr>


            <td colspan="2">


              <p class="mb-0"><b>Compétences de discipline :</b></p>


              <ul class="mb-0">


                  <li>Analyser un problème en termes de flux et de traitement d’informations</li>


                  <li>Décomposer un problème en sous-problèmes, reconnaître des situations déjà analysées</li>


                  <li>Comprendre et réutiliser des codes sources existants</li>


                  <li>Mobiliser les concepts et les technologies utiles pour assurer les fonctions de mémorisation et de traitement des informations</li>


              </ul>


              <p class="mb-0 mt-2"><b>Compétences transversales :</b></p>


              <ul class="mb-0">


                  <li>Faire preuve d’autonomie, d’initiative et de créativité</li>


                  <li>Présenter un problème ou sa solution</li>


                  <li>Coopérer au sein d’une équipe</li>


                  <li>Rechercher de l’information, apprendre à utiliser des sources de qualité, partager des ressources</li>


              </ul>


            </td>


        </tr>


    </tbody>


</table>



<table class="table table-sm">


    <thead>


        <tr><td class="align-middle text-center" style="width:0" colspan="100"><b>Environnement à préparer</b></td></tr>


    </thead>


    <tbody>


        <tr>


            <td class="text-center" style="width:0"><b>Aménagement de l'espace</b></td>


            <td><ul class="mb-0">


              <li>Par défaut, la salle dispose les élèves en ilôts</li>


            </ul></td>


        </tr>


        <tr>


            <td class="text-center" style="width:0"><b>Matériel</b></td>


            <td><ul class="mb-0">


                <li>Enseignant : ordinateur + video-projecteur</li>


                <li>Élèves : un ordinateur par élève</li>


            </ul></td>


        </tr>


        <tr>


            <td class="text-center" style="width:0"><b>Software</b></td>


            <td><ul class="mb-0">


                <li>Élèves : IDE thonny</li>


            </ul></td>


        </tr>


        <tr>


            <td class="text-center" style="width:0"><b>Support(s) utilisé(s)</b></td>


            <td><ul class="mb-0">


                <li>Enseignant & Élèves : <ul>


                  <li>+++lien "/apprenant/01-__-_NSI-_premiere/08-__-_representation-_des-_entiers-_relatifs-_et-_reels-_en-_binaire-_et-_tri-_et-_fusion-_de-_donnees-_dans-_une-_table/cours/01-__-_tri-_et-_fusion-_de-_donnees-_dans-_une-_table.md" "<b>[cours & activité(s)]</b> Tri et fusion de données dans une table" (disponible en ligne)</li>


                </ul></li>


            </ul></td>


        </tr>


    </tbody>


</table>



### Déroulement prévu de la séance



<table class="table table-sm il-table-collapse-2">


    <tbody>


        <tr>


            <td class="align-middle text-center text-nowrap w"><b>#0</b></td>


            <td colspan="3"><i class="isFile fas fa-file-alt"></i> <a href="/apprenant/01-__-_NSI-_premiere/08-__-_representation-_des-_entiers-_relatifs-_et-_reels-_en-_binaire-_et-_tri-_et-_fusion-_de-_donnees-_dans-_une-_table/cours/01-__-_tri-_et-_fusion-_de-_donnees-_dans-_une-_table.md" target="_blank">Introduction de la séance</a></td>


            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>


            <td class="align-middle text-center text-nowrap" style="width:0">00h00 -> 00h10</td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>


            <td colspan="10"><ol class="mb-0" start="0">


              <li>Présentation des objectifs de la séance</li>


              <li>Rappels de ce que nous avons appris lors de séances antérieures. Le but est de bien différencier protocole de communication et données et pourquoi nous pouvons être amenés à utiliser un langage de programmation pour manipuler des données</li>


            </ol></td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>


            <td colspan="10"><ul class="mb-0">


                <li>Enseignant : cours projeté au tableau</li>


            </ul></td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>


            <td colspan="10"><ul class="mb-0">


                <li>Enseignant : video projecteur + ordinateur portable</li>


            </ul></td>


        </tr>


    </tbody>


</table>



<table class="table table-sm">


    <tbody>


        <tr>


            <td class="align-middle text-center text-nowrap w"><b>#1</b></td>


            <td colspan="3">Présentation générale du prochain projet</td>


            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>


            <td class="align-middle text-center text-nowrap" style="width:0">00h10 -> 00h15</td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>


            <td colspan="10"><ol class="mb-0" start="0">


              <li>Le contenu de cette séance fera partie du cœur du prochain projet. Présenter le projet ici permet d'impliquer davantage les élèves pour la suite de la séance</li>


              <li>Plusieurs idées de projets, gravitant autour de l'exploitation d'un fichier CSV, sont proposées. Les élèves sont incités à réfléchir et à me soumettre leurs propres idées pendant les vacances qui arrivent après cette séance</li>


            </ol></td>


        </tr>


    </tbody>


</table>



<table class="table table-sm il-table-collapse-2">


    <tbody>


        <tr>


            <td class="align-middle text-center text-nowrap w"><b>#2</b></td>


            <td colspan="3"><i class="isFile fas fa-file-alt"></i> <a href="/apprenant/01-__-_NSI-_premiere/08-__-_representation-_des-_entiers-_relatifs-_et-_reels-_en-_binaire-_et-_tri-_et-_fusion-_de-_donnees-_dans-_une-_table/activites/applications-_directes/tri-_de-_donnees-_en-_table.md" target="_blank">Activité d'introduction centrée sur l'analyse et la modification d'un programme</a></td>


            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>


            <td class="align-middle text-center text-nowrap" style="width:0">00h10 -> 00h30</td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>


            <td colspan="10"><ol class="mb-0" start="0">


              <li>Les élèves démarrent en autonomie cette première activité de séance. Le but principal de cette activité est de mettre en avant le fait qu'il faille élaborer un programme contenant des dizaines de lignes pour faire une manipulation simple sur un fichier CSV. Cette activité permet également d'observer une application concrète d'un algorithme de tri.</li>


              <li>Mes interventions varient en fonction des avancées individuelles et collectives de la classe</li>


              <li>L'activité étant courte, je m'assure que l'ensemble des élèves se soient penchés sur la dernière question (la question 3) quelques minutes avant d'entamer une correction orale avec l'ensemble de la classe</li>


              <li>Des questions sont prévues pour les élèves qui avancent plus vite</li>


              <li>La correction est une occasion avec les élèves de mettre en lumière certains points importants liés à la programmation : <ul>


                <li>Lors de l'analyse d'un programme : observer les noms des fonctions et des variables, lire les commentaires... avant de rentrer dans une analyse plus détaillée</li>


                <li>Ce qui permet de mettre en avant certaines bonnes pratiques de programmation (structure d'un programme, nommage, commentaires, ...)</li>


              </ul> </li>


            </ol></td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>


            <td colspan="10"><ul class="mb-0">


                <li>Enseignant : activité et corrections projetées au tableau</li>


            </ul></td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>


            <td colspan="10"><ul class="mb-0">


                <li>Enseignant : video projecteur + ordinateur portable</li>


                <li>Élèves : ordinateur</li>


            </ul></td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>


            <td colspan="10"><ul class="mb-0">


                <li>La question 3, qui consiste à analyser un programme d'une quarantaine de lignes, présente dans cette activité la principale difficulté pour les élèves. J'interviens donc ponctuellement auprès de ceux semblant bloqués pour les accompagner dans leur analyse du programme présenté dans l'activité</li>


            </ul></td>


        </tr>


    </tbody>


</table>



<table class="table table-sm il-table-collapse-2">


    <tbody>


        <tr>


            <td class="align-middle text-center text-nowrap w"><b>#3</b></td>


            <td colspan="3">Bilan de l'activité et échanges avec les élèves sur l'utilisation de bbliothèques</td>


            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>


            <td class="align-middle text-center text-nowrap" style="width:0">00h30 -> 00h35</td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>


            <td colspan="10"><ol class="mb-0" start="0">


              <li>Je mets en lumière avec les elèves le fait qu'une approche from scratch peut rapidement générer des programmes contenant un nombre important de lignes. Ce qui permet d'introduire l'utilisation par la suite de la bibliothèque panda</li>


              <li>Un cours rappel est fait sur ce qu'est une bibliothèque en programmation</li>


            </ol></td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>


            <td colspan="10"><ul class="mb-0">


                <li>Enseignant : cours projeté au tableau</li>


            </ul></td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>


            <td colspan="10"><ul class="mb-0">


                <li>Enseignant : video projecteur + ordinateur portable</li>


            </ul></td>


        </tr>


    </tbody>


</table>



<table class="table table-sm il-table-collapse-2">


    <tbody>


        <tr>


            <td class="align-middle text-center text-nowrap w"><b>#4</b></td>


            <td colspan="3"><i class="isFile fas fa-file-alt"></i> <a href="/apprenant/01-__-_NSI-_premiere/08-__-_representation-_des-_entiers-_relatifs-_et-_reels-_en-_binaire-_et-_tri-_et-_fusion-_de-_donnees-_dans-_une-_table/activites/applications-_directes/tri-_et-_fusion-_de-_donnees-_en-_table-_avec-_pandas.md" target="_blank">Activité de découverte sur la bibliothèque python pandas (bibliothèque facilitant les manipulations de fichier CSV)</a></td>


            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>


            <td class="align-middle text-center text-nowrap" style="width:0">00h35 -> 01h45</td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>


            <td colspan="10"><ol class="mb-0" start="0">


              <li>Les élèves démarrent en autonomie l'activité. Cette activité a deux buts : apprendre à utiliser la bibliothèque pandas (pour trier et fusionner des tables) et apprendre à se documenter sur le Web sur l'utilisation d'une bibliothèque</li>


              <li>Les corrections aux questions de cette activité ne sont pas disponibles pendant la séance</li>


              <li>Mes interventions varient en fonction des avancées individuelles et collectives de la classe. J'invite les élèves à se lever pour aider ou demander de l'aide</li>


              <li>Les élèves en avance peuvent commencer à travailler sur le futur projet</li>


            </ol></td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>


            <td colspan="10"><ul class="mb-0">


                <li>Enseignant : activité et corrections projetées au tableau</li>


            </ul></td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>


            <td colspan="10"><ul class="mb-0">


                <li>Enseignant : video projecteur + ordinateur portable</li>


                <li>Élèves : ordinateur</li>


            </ul></td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>


            <td colspan="10"><ul class="mb-0">


                <li>La longue durée de l'activité et la recherche, par soi même, de documentation sur le Web sont je pense les points qui poseront le plus de difficultés aux élèves durant cette activité : <ul>


                  <li>J'invite les élèves passifs à se lever et à aller observer le travail des autres camarades</li>


                  <li>J'accompagne les élèves en difficulté dans leur recherche sur le Web et analyse avec eux les mots-clés utilisés lors de leurs recherches. Les bons mots-clés permettent d'arriver directement sur les bons sites où se trouvent les réponses aux questions de l'activité</li>  


                </ul></li>


            </ul></td>


        </tr>


    </tbody>


</table>



<table class="table table-sm il-table-collapse-2">


    <tbody>


        <tr>


            <td class="align-middle text-center text-nowrap w"><b>#5</b></td>


            <td colspan="3">Bilan de la séance et présentation du prochain projet</td>


            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>


            <td class="align-middle text-center text-nowrap" style="width:0">01h45 -> 01h50</td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:110px"><b>contenu</b></td>


            <td colspan="10"><ol class="mb-0" start="0">


              <li>Ces 5 minutes sont importantes, donc si besoin la deuxième activité sera arrétée en cours de route pour assurer cette étape</li>


              <li>Un rappel est fait sur les idées de projets proposés, sur la possibilité de proposer son projet et sur les modalités des futures séances "projet" qui arrivent</li>


              <li>Nous échangeons tous ensemble pour répondre aux éventuelles questions</li>


            </ol></td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>


            <td colspan="10"><ul class="mb-0">


                <li>Enseignant : cours projeté au tableau</li>


            </ul></td>


        </tr>


        <tr>


            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>


            <td colspan="10"><ul class="mb-0">


                <li>Enseignant : video projecteur + ordinateur portable</li>


            </ul></td>


        </tr>


    </tbody>


</table>


