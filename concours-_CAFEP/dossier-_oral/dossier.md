# Présentation d'activités pour la séquence "Base de données" de Terminale NSI

<div class="d-print-none p-3 mb-2 bg-danger text-white">Ici se construit le dossier qui sera transmis aux jurys avant l'oral. Le texte <span class="text-blue">écrit en bleu</span> fait partie de mes notes personnelles et ne figurera pas dans le dossier transmis aux jurys.</div>

<div class="d-print-none text-blue">
<p>Le dossier doit suivre les quelques règles suivantes : </p>
<ul>
    <li>Doit etre rendu le lundi 14 Juin 23h59 dernier delais sous format <b>PDF</b></li>
    <li>Doit contenir de 4 à 6 pages MAX</li>
    <li>Doit être envoyé à l'adresse dossier@capes-nsi.org, le corps du mail doit contenir : mon nom, date de naissance et numero de candidat</li>
    <li>Proposer de développer une à plusieurs activités pédago</li>
    <li>Décrire les connaisances et compétences visées</li>
    <li>Le dossier sera à défendre pendant 30mn à l'oral en totalité ou partiellement</li>
    <li>diapo / annexes , doit être fournis dans un zip de 20Mo max au plus le mardi 22 a 23h59 (dossier@capes-nsi.org)</li>
</ul>
</div>

L'introduction aux bases de données en Terminale NSI permet une appropriation plus progressive par les élèves de ce domaine qu'auparavant.

En effet, durant les études supérieures, il est bien compliqué pour un enseignant de faire de ses étudiants des experts en bases de données en l'espace de quelques mois. Il s'agit d'un domaine complexe dont les enjeux et contenus ont besoin parfois d'un certain temps de pratique et de maturation dans les esprits des apprenants avant d'être correctement assimilés. 

Il y a cet adage que j'affectionne particulièrement "il est simple de faire compliqué, et compliqué de faire simple". Cela me semble particulièrement vrai dans l'enseignement des concepts liés aux bases de données. Pour ces raisons, j'ai décidé de consacrer ce dossier sur ce thème et de concevoir des activités intervenant à des moments clé au sein d'une séquence dédiée.

Le terme "activité" est utilisé ici dans un sens assez large ; il s'agira d'une phase, au temps variable, à but pédagogique durant laquelle les élèves en seront les principaux acteurs. Cette phase met en lumière une ou plusieurs problématiques auxquelles les élèves apporteront des réponses tout en évoluant dans un cadre établi. Le cadre sera plus ou moins dirigé en fonction de la nature de l'activité ; elle pourra être très guidée lorsqu'elle introduira de nouvelles notions de cours ou à l'inverse peu voir pas du tout guidée durant les activités avancées et pendant le projet.

Bon nombre de sites Web, groupes de réseaux sociaux et manuels proposent déjà aujourd'hui des activités adaptées pour le thème "Base de données" de terminale NSI. J'ai néanmoins à souhait ici de proposer des activiés créées par mes soins. Pour la suite de la carrière d'enseignant dans laquelle je me projette, il sera plus facile d'apporter des activités actualisées et adaptées en fonction du publique auquel je m'adresse en m'entrainant justement à créer des activités de toute pièce.

## Structure de la séquence

Avant la conception des activées, il me semble important de poser quelques caractéristiques essentielles de la séquence dans laquelle elles s'inscriront. Suite à divers calculs dépendant de plusieurs facteurs (nombre de thèmes en Terminale NSI, épreuve écrite en Mars, ...) j'estime que je peux mobiliser les élèves sur ce thème pendant une douzaine de séances de deux heures. Ce qui donne, à raison de 6 heures de NSI par semaine, 4 semaines de bases de données. [['Séquence : nombre de Séances', annexe nº2](http://91.166.146.79:33446/enseignement/concours/dossier-_oral-_2/annexes.md#2squencenombredesances)]

Après analyse du programme officiel et des sujets disponibles sur le site [https://eduscol.education.fr](https://eduscol.education.fr), je pose sur papier les principaux objectifs de mes séances [['Séquence : organisation des séances', annexe nº3](http://91.166.146.79:33446/enseignement/concours/dossier-_oral-_2/annexes.md#3squenceorganisationdessances)]. Je constate que je peux consacrer 6 séances sur le cours et exercices d'applications directes, j'obtiens en reste 6 autres séances. Ces dernières seront parfaites pour mettre en place un projet d'envergure et stimulant avec les élèves. 

Maintenant toutes ces informations en main, je vais pouvoir m’atteler à la conception des activités. Le thème est vaste, les activités possibles sont nombreuses, Je vais donc me concentrer sur plusieurs moments clé intervenants dans une séquence : 
- Lorsque se déroule la toute première séance où le thème est introduit
- Lorsqu'un points difficile du thème est abordé
- Lors des phases de lancement et de réalisation d'un projet

Je décide donc de présenter dans ce dossier : 

- Une activité nommée "activité introductive" introduisant le thème "Bases de données"
- Une activité nommée "activité intermédiaire" consacrée à une notion difficile du thème : les requêtes de jointure de tables
- Deux activités contenues dans les séances du projet "air quality control" ; une durant un des TPs introduisant le projet et une autre durant la phase de réalisation du projet

## Activité introductive de séquence

Il est essentiel que les élèves comprennent dès le départ les enjeux techniques qui ont fait naître le domaine des bases de données. Ils sont multiples ; temps de stockage et de récupération de données, maintenance et évolution de la structure des données, utilisation concurrentielle des données, etc..

Je décide de concentrer cette toute première activité de séquence sur les problématiques les plus simples à mettre en évidence : le temps de stockage et de récupération de données lorsque ces dernières deviennent (trop) volumineuses.

Je dois donc amener les élèves à manipuler des données inscrites dans un fichier texte au format .csv comme ils ont pu le faire en première. À la différence que ce fichier .csv sera volontairement (très) volumineux. Les élèves constateront à travers les manipulations les limites du traitement de données volumineuses en étant seulement armé de python et d'un fichier .csv. Ce constat entrainera un enchainement naturellement sur une introduction aux bases de données.

Il n'est pas évident de trouver sur Internet un fichier .csv en libre accès, contenant des données parlantes pour des lycéens, et volumineuses. 
 J'opte finalement pour générer moi même ces données en les basant sur une information réelle : le nombre reccord de visite sur le site leboncoin twitté par le directeur général de la plateforme

![https://twitter.com/ajouteau/status/1359080640122093572?ref_src=twsrc%5Etfw](https://i.ibb.co/vJG49DV/Screenshot-from-2021-04-18-06-38-57.png)

À partir de cette information et de suppositions je peux générer et partager un fichier .csv contenant un ensemble factices de données relatives aux achats d'utilisateurs. Faire générer ce .csv volumineux par les élèves est peu pertinent d'un point de vue pédagogique. Cependant, les inviter à effectuer la même démarche que j'ai eu pour construire le fichier peut faire l'objet d'un exercice très ludique : "comment estimer le nombre de vente du site à partir d'un tweet ?".

Le déroulement de l'activité devient clair :
1. Au tout début de la séance, j'expose le tweet du directeur général du site web "leboncoin"
2. Je présente aux élèves l'activité et son but : se confronter à un problème technique dont la solution sera apportée par le sujet du thème
3. J'invite les élèves à réfléchir et à estimer le nombre d'achat effectué sur le site en fonction de durées spécificiques (par jours, par mois, ...)
4. Je fais déduire aux élèves le nombre d'entrées d'un fichier .csv et leur fournir ce fichier .csv
5. J'invite les élèves à effectuer une simple recherche dans le fichier .csv, de taille volumineuse mais raisonnable, à l'aide d'un script python à compléter et à noter le temps d'exécution du script
6. Après une mise en commun des temps d'exécutions notés, nous poursuivons la réflexion sur des fichiers plus volumineux puis concluons

Durant cette activité, les élèves évolueront probablement à des rythmes différents, notamment lorsqu'il faudra compléter le script python. Je mettrais alors dès le début en place des binômes afin de favoriser l'entraide et l'autonomie. Cela me permettra plus facilement d'intervenir auprès des élèves en difficulté tout en limitant la pratique d'une pédagogie "de garçon de café".

Une fiche "activité introductive" sera également fournie et sera à compléter tout au long de l'activité. Les élèves devront y rédiger notamment des rappels important du programme de première. Cette fiche me permet aussi de guider les élèves de manière efficace, cela afin de démarrer au plus tôt le premier cours dédié aux bases de données. [['Activité introductive : fiche support', annexe nº4](http://91.166.146.79:33446/enseignement/concours/dossier-_oral-_2/annexes.md#4fichesupportactivitintroductive)]

Certaines connaissances provenant de première, très utiles au programme de terminale, sont réinvestis :
- La définition de ce qu'est le format .csv sera rappelée et sera très utile pour la suite de la séquence
- Le recherche d'une information dans un fichier .csv se fera à l'aide d'un script python. Donc en plus de s'exercer sur le langage phare du programme du lycée, les élèves (re)verront comment manipuler un fichier texte dans un programme python

Et durant cette activité, les élèves consolideront les compétences, du programme, suivantes : 
- mobiliser les concepts et les technologies utiles pour assurer les fonctions [...] de mémorisation, de traitement [...] des informations
- (transversale) rechercher de l'information, partager des ressources

## Activité intermédiaire de séquence

Nous entrons dans la cinquième séance de la séquence. Ici sont introduits les concepts gravitant autour de la gestion de base de données contenant des tables liées entres elles. Ces concepts, importants, sont présents dans le programme officiel : la définition et utilisation de clé étrangère ainsi que la rédaction de requêtes de jointures.

Comme énoncé dans l'introduction de ce dossier, ce thème a besoin d'être pratiqué pour permettre une assimilation plus efficace de la part des élèves. Cela me semble d'autant plus pertinent à ce moment où nous abordons des points délicats du programme. 

C'est donc la raison pour laquelle je souhaite que les élèves prennent une part centrale dans le déroulement de cette séance. Ainsi. à travers une démarche inductive, je prend le parti de mettre à la disposition des élèves une base de donnée contenant deux tables liées et déjà peuplées. Je fournierai également un petit ensemble de requêtes utilisant des jointures. À partir de ces données, les élèves auront pour consigne d'analyser les structures et requêtes fournis puis en déduire quelles informations "semblent" permettre d'extraire des données contenues dans différentes tables.

Par contre, comment les élèves vont comprendre l'intéret d'utiliser plusieurs tables ? un élève pourrait très bien me demander : pourquoi ne pas stocker toutes les données dans une seule table ? 

En anticipant cette question, je trouve ici un moment idéal dans ma séquence pour exposer aux élèves les anomalies qui peuvent se présenter dans une base de données. 




~ en cours de réflexion/rédaction ~

- utiliser mon site pour cette activité
- differenciation pedagogique 
    - evaluation formative pour jauger les niveaux
    - doc : [lien](https://innovation.sainteanne.ca/wp-content/uploads/2017/11/Differenciation.pdf)

## Activités pour le projet "air quality control"

Dans l'industrie informatique, les données stockées dans des bases doivent souvent in fine être exploitées par des utilisateurs non techniques. Fort de leurs connaissances et compétences accumulées depuis la première NSI et lors de cette séquence, les élèves vont devoir créer une application composée d'une base de données et d'une interface graphique.

Le but du projet "air quality control" est de permettre à des utilisateurs de consulter des données en lien avec la composition environnante de l'air. La classe disposera du matériel suivant : un raspberry pi, un module "sensor hat" permettant de collecter la température ambiante ou encore la pression, et un module "SDS011" permettant de capter le taux ambiant de particules fine. Sur le raspberry sera créé un espace personnel, accessible via connexion ssh, pour chaque élève. Cet espace permettra, entre autres, d'accéder aux données collectées par les modules "Sensor Hat" et "SDS011".

![https://i.ibb.co/bbJQsxV/IMG-20210502-0941221792.jpg](https://i.ibb.co/bbJQsxV/IMG-20210502-0941221792.jpg)

Avant de présenter les deux activités que j'ai choisi, voici l'organisation générale du projet dans lequel elles se déroulent :
1. (séance 1) Partage des résultats des recherches sur la qualité de l'air, description du projet et supports utilisés
2. (séance 1) Analyse des données collectées par les modules, puis, conception et création d'une base de donnée adaptée
3. (séance 2) Développement d'un script python pour peupler automatiquement la base de données
4. (séances 3, 4 et 5) Conception et création d'une interface graphique pour afficher les données stockées
5. (séance 6) Présentations orales des projets

### Activité du projet nº1 : restitution orale des recherches sur les informations liées à la qualité de l'air

Les élèves seront amenés à manipuler diverses données relatives à la composition et qualité de l'air. Certaines seront peu familières comme le taux de particules fines PM2.5 et PM10. Ainsi, les élèves devront faire des recherches en amont chez eux avant de démarrer la première séance du projet. L'activité, présentée ici et essentiellement orale, a pour principal objectif de faire restituer et mettre en commun les résultats des recherches des élèves.

La qualité de l'air est un sujet vaste donc afin d'aiguiller les élèves dans leurs recherches, une fiche à compléter leur sera fournis [['Activité introductive : fiche support', annexe nº6](http://91.166.146.79:33446/enseignement/concours/dossier-_oral-_2/annexes.md#6fichesupportqualitdelair)]. Les élèves pourront également guider leurs recherches en s'aidant du cahier des charges du projet fournis aussi en amont. Dans une optique de sensibilisation aux enjeux environnementaux, il leur sera aussi demandé de rechercher des informations sur une lois Française en rapport avec le controle de la qualité de l'air.

Lorsque démarrera la toute première séance consacrée au projet, 3 élèves choisis devront animer devant la classe une discussion pour présenter le résultat de leurs recherches. Cette consigne sera donnée lors de la distribution des fiches "qualité de l'air" et écrite sur ces dernières. Cette activité sera l'occasion de rappeler quelques règles élémentaires pour mener correctement un exercice oral en vue de l'épreuve du grand oral de fin d'année.

La mise en place de cette activité présente deux difficultés : 
1. Faire le bon choix des élèves pour s'assurer que la discussion soit vivante, efficace et prolifique
2. Garder le reste de la classe active pendant l'activité

Concernant le premier point, assigner à un des 3 élèves le rôle d'arbitre de la discussion permettrait d'investir d'avantage la classe dans cet exercice en étant plus autonome. Cet arbitre devra reprendre les differentes questions de la fiche "qualité de l'air", répartir la parole dans la discussion et de noter les réponses et informations intéressantes au tableau, j'accompagnerai l'arbitre dans sa démarche si besoin. Ce rôle pourrait être assigné à un élève n'ayant pu effectuer les recherches quel qu'en soit la raison. À contrario, il faudra s'assurer que les deux autres élèves aient bien effectués les recherches et complété leur fiche de leur côté. Cela pourrait être une excellente occasion pour d'entraîner un élève timide mais assidu dans cette discussion.

Et au sujet de la deuxième difficulté, l'arbitre de la discussion devra également donner de manière ponctuelle la parole aux élèves assis. Cela peut se passer lors d'un temps mort durant la discussion, ou lorsqu'une information donnée qui pourrait être complétée, etc.. . J'accompagnerai aussi l'arbitre dans cette démarche si besoin. Un déroulement efficace de la discussion doit être assuré afin que l'activité ne s'étire pas trop sur le temps, les élèves assis perdraient peu à peu leur concentration et l'objectif de la séance est de démarrer le projet en priorité. Il sera donc donné comme consigne aux élèves de surveiller le temps et de ne pas exéder les 10 à 15 minutes de discussions.

Une fois cet exercice oral terminé, les élèves complèront leurs fiches avec les informations synthétisées au tableau. Après quoi je reprendrai la main pour exposer dans les détails le projet et son organisation.

Cette courte activité s'éloigne de la thématique du thème mais aborde pour autant des points importants, tels que des compétences transversales décrites dans le programme :
- Faire preuve d'autonomie
- Développer une argumentation dans le cadre d'un débat (notamment lorsque les élèves parlerons des lois Françaises qu'ils ont trouvées)
- Rechercher de l'information, partager des ressources

Et permet par ailleur de sensibiliser les élèves, dans ce projet, sur les enjeux sannitaires décrits dans le parcours éducatif de santé.

### Activité du projet nº2 : 

~ en cours de réflexion/rédaction ~
- fusion des binomes 
- differenciation pedagogique 
- pedagogie par projet
- placement dans la classe en ilot


