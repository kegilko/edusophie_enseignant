# cahier des charges du projet Air Quality Controller

+++sommaire

## Présentation du document

Différentes personnes de tous bords peuvent intervenir dans un projet informatique. Un cahier des charges permets de poser sur papier les attendus des uns (le(s) commanditaire(s) du projet) et les environnements et stratégies mis en places pour concrétiser le projet. Il s'agit en règle générale d'un document conséquent évoluant au fil du projet. 

Ce cahier des charges, de taille modeste, ne changera pas.

Pour terminer, un court descriptif des différentes parties du projet. 
- **Présentation du projet** : contextualise et présente le projet dans ses grandes lignes
- **Spécifications fonctionnelles** : cette partie décrit le fonctionnement de l'application depuis le point de vue d'un utilisateur non technique.
- **Spécifications techniques** : cette partie rassemble les informations techniques permettant de mettre en oeuvre le projet 
- **Planification du projet** : cette partie expose les différentes phases de développement du projet
- **Glossaire** : Ce document contiendra sans doute des <u>termes</u> qui vous sont inconnus. N'hésitez pas à jeter un coup d'oeil au glossaire ! il n'est pas nécessaire d'apprendre ces termes.

## Présentation du projet

<blockquote class="blockquote">
  <p>L’éducation à l’environnement et au développement durable est porteuse d’enjeux essentiels en termes d’évolutions des comportements, de connaissances nouvelles et de mise en capacité de chacun, au quotidien, d’être acteur de la transition et de la mise en œuvre des objectifs de développement durable.</p>
  <footer class="blockquote-footer"><cite title="Source Title">https://www.ecologie.gouv.fr/leducation-lenvironnement-et-au-developpement-durable</cite></footer>
</blockquote>

L'activité environnente et humaine influent sur la composition de l'air. Cette composition peut impacter de manière néfaste la santé des organismes vivants. 

Le but du projet "**A**ir **Q**uality **C**ontroller" est de développer un site web permettant de consulter diverses données et statistiques liées à l'état de l'atmosphère et à la composition de l'air. 

## Spécifications fonctionnelles

### Structure du site

+++ diagramme
graph TD
    A[Page d'accueil] -->|lien vers| B(Page 'Module : sensor hat')
    A[Page d'accueil] -->|lien vers| C(Page 'Module : nova SDS011')
    A[Page d'accueil] -->|lien vers| D(Page 'Statistiques')
+++

### Liste des fonctionnalités du site

#### Fonctionnalités obligatoires

Le site devra proposé au minima les fonctionnalités suivantes : 

- afficher les informations courrantes liées au module 1 'sensor hat' dans une page dédiée titrée '**Module : sensor hat**'. Les informations attendues sont : 
    - la température actuelle (en **°C**, seulement la partie décimale)
    - la pression atmosphérique actuelle (en **Pa**) 
    - le taux d'humidité actuel (en **%**)
- afficher les informations liées au module 2 'nova SDS011' dans une page dédiée titrée '**Module : nova SDS011**'. Les informations attendues sont : 
    - le taux de particules fines actuel inférieures à 10μm (en **μg/m3**)
    - le taux de particules fines actuel inférieures à 2.5μm (en **μg/m3**)
- afficher des statistiques dans une page dédiée titrée '**Statistiques**'. Les statistiques attendues sont les suivantes :
    - afficher un tableau nommé '**évolution générale**' regroupant les informations, des deux modules, captées depuis 3 jours. 

Le site devra être déployé sur un serveur dédié.

#### Fonctionnalités optionnelles

Les fonctionnalités ci-dessous, **optionnelles**, demandent un peu plus de doigté pour être développées. Il vous est possible de proposer des idées qui devront être validées.

Fonctionnalité optionnelle 1 : 
- Depuis la page '**Statistiques**', ajouter des statistiques permettant à l'utilisateur de comparer rapidement la différence entre le taux moyen de particules fines en semaine (lundi-vendredi) et le taux moyen de particules fines pendant le week end. (samedi-dimanche). 

Fonctionnalité optionnelle 2 : 
- Depuis la page '**Statistiques**', ajouter un graphique en courbe (📉) qui affiche de manière graphique les données contenues dans le tableau '**évolution générale**'. Vous devrez chercher, trouver et vous aider d'une librairie javascript pour afficher le graphique en courbe.

Fonctionnalité optionnelle 3 :
- Depuis la page d'accueil, afficher un 'air-quality-control-score' qui, se servant des dernières données récoltées par les modules, affiche une note reflétant la qualité de l'air. Cette note ira de A (qualité optimale) à E (qualité exécrable). Ce score s'inspire du [nutri-score](https://www.mangerbouger.fr/Manger-mieux/Comment-manger-mieux/Comment-comprendre-les-informations-nutritionnelles/Qu-est-ce-que-le-Nutri-Score) que l'on trouve sur certain produits consommables dans le commerce. Dans un soucis de transparance vis à vis de l'utilisateur, Vous devrez également afficher les raisons qui expliquent le score obtenu.

### Exemple de maquettes

Ci-dessous un exemple de <u>maquettes</u> pour le site. Il s'agit d'exemples qui peuvent servir à vous inspirer lors de la création du site. Vous êtes libre d'adopter votre propre charte graphique.

~à compléter~

## Spécifications techniques

### Architecture générale

Ci-dessous le diagramme représentant l'intéraction entre le visiteur du site web (l'utilisateur) et le serveur.

+++ diagramme
flowchart LR
    A[navigateur web]-->|sollicite la page d'accueil| C[serveur HTTP]
    C-->|envoie la page demandée| A
    subgraph serveur
    C[serveur HTTP applicatif]
    C-->|construit la page demandée|C
    end
    subgraph client
    A[navigateur web]
    end
+++

Puis une vue un peu plus détaillées côté serveur. 

+++ diagramme
flowchart LR
    subgraph serveur
    C-->|construit la page demandée|C
    D-->|envoie les données sollicitées|C
    C[serveur HTTP applicatif]-->|sollicite si besoin des données|D[Base de données SQL]
    end
+++

Nous remarquons que dans ce schéma, le serveur http applicatif semble contenir tous le code du site. Concentrer tous le code dans un même endroit n'est jamais une bonne idée.

Il est donc **vivement** conseillé de mettre en place l'architecture applicative suivante : 

+++ diagramme
flowchart LR
    subgraph serveur
    A[serveur HTTP applicatif `serveurHTTP.py`]<-->B[`page_home.py`]
    A<-.->C[`page_sensorhat.py`]
    A<-.->D[`page_novaSDS011.py`]
    B<-.->E[Base de données SQL]
    C<-.->E
    D<-.->E
    end
+++

### Environnement technique

Ci-dessous est décrit l'environnement technique sur lequel vous travaillerez pendant vos développements ainsi que son utilisation.

Le raspberry PI est un SBC (single board computer) qui utilise dans sa 4ième version un système d'exploitation au nom très original "Raspberry PI OS". Ce système d'exploitation se repose sur un noyau Linux. Les lignes de commandes seront donc très similaires aux lignes de commandes utilisées dans d'autres systèmes d'exploitation Linux connus tels que Ubuntu ou encore Debian.

#### Le raspberry : récolter les données nécessaires au site

Les données liées à l'état de l'atmosphère et la composition de l'air sont enregistrées sur un raspberry PI. (version 4)

![](https://i.ibb.co/vkcBJtk/01.jpg)

... avec l'aide du module "sensort hat" 

![](https://i.ibb.co/gwLr4B0/02.jpg)

... et du module "nova SDS011". 

![](https://i.ibb.co/2n5B8F2/03.jpg)

Le module "sensorthat" permet de capturer les données suivantes : la température (en **°C**), la pression atmosphérique (en **hPa**) et le taux d'humidité (en **%**)

Et le module "nova SDS011" permet de capturer les données suivantes : le taux de particules **PM2.5** (en **μg/m3**) et le taux de particules **PM10** (en **μg/m3**)

Pour accéder aux données du raspberry, **vous devrez vous y connecter via le protocol ssh** depuis un poste informatique connecté sur le même réseau. Le raspberry est déjà configure pour accepter des connexion entrantes à travers le protocol ssh. Si votre poste informatique est sur Windows, nous vous conseillons d'utiliser le logiciel freesource [putty](https://www.putty.org/).

Vous aurez besoin d'un **identifiant** et d'un **mot de passe** pour mener à bien votre connexion ssh, rapprochez vous de votre enseignant pour les obtenir. Vous aurez également besoin de l'adresse IP du raspberry, elle est affichée sur l'étiquette attachées au raspberry. 

#### Le raspberry : déployer un serveur web

Lors de vos développements, vous pourrez bien entendu utiliser votre poste informatique personnel. Cependant, **il est vivement déconseillé** d'attendre la fin du projet pour tester le déploiement du site sur un serveur distant.

Le raspberry PI va également être utilisé pour faire office de serveur web. Vous devrez donc copier le code de votre site sur le raspberry et démarrer le serveur `serveurHTTP.py` sur un port non utilisé. Il vous suffira ensuite d'ouvrir votre navigateur préféré, depuis un poste connecté sur le même réseau que le raspberry, et de saisir l'url "http://`adresse ip du raspberry`:`port du serveur`"

## Planification du projet

Le développement du projet est découpé en 3 phases.

### Phase 1 : conception et création de la base de données "AQC"

Durée de la phase : 1 séance de 2 heures

À la fin de cette phase, vous devrez : 
- avoir rédigé le schéma relationnel de votre base de données
- avoir créé la base de données nommée "AQC" avec une table par module (une table "sensortHat" et une table "novaSDS011")
- avoir ajouté quelques données dans la base de données
- avoir complété le dossier de suivis de projet (partie "Phase 1")
- (TODO : à trancher) avoir crée un repo gitlab
- (TODO : à trancher) avoir sauvegardé votre travail dans un repo

### Phase 2 : peuplement et exploitation de la base de données "AQC"

Durée de la phase : 1 séance de 2 heures

À la fin de cette phase, vous devrez : 
- avoir sauvegardé toutes les données récoltés par le raspberry
- avoir intégré toutes les données dans votre base de données
- avoir déterminer quelles requêtes vont être nécessaires pour let site
- avoir rédigé et testé (en partie ou toutes) les requêtes nécessaires pour le site
- avoir complété le dossier de suivis de projet (partie "Phase 2")
- (TODO : à trancher) avoir sauvegardé l'avancée de votre travail dans un repo

### Phase 3 : développement de l'interface graphique web

Durée de la phase : 3 séances (séance 1, séance 2 et séance 3) de ~2 heures

Au début de chaque séance vous devrez : 
- avoir complété le dossier de suivis de projet (partie "Phase 3" séance X "répartition des rôles", où X est le numéro de la séance)

À la fin de cette phase, vous devrez : 
- avoir développé les <a href="#fonctionnalitsobligatoires">fonctionnalitées obligatoires</a> attendues 
- (TODO : à trancher) avoir sauvegardé la version finale de votre travail dans un repo
- avoir déployé le site sur le raspberry
- (optionnel) avoir développé une fonctionnalité supplémentaire (une fonctionnalité que vous proposez, ou, une fonctionnalité parmis celles exposées dans les <a href="#fonctionnalitsoptionnelles">Fonctionnalités optionnelles</a>)


## Glossaire

- maquette :
- site statique : 
