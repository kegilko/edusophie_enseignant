# question jury

ORAL 2 CAPES NSI
QUESTIONS 

+++sommaire

Les questions ci-dessous ont été reprises depuis [ce travail collaboratif](https://annuel2.framapad.org/p/questionsoral2-9mm9?lang=fr). Pour chacunes d'entre elles : 
- je donne une réponse et ajoute un ⚠️ **TODO** ⚠️ si il y a besoin d'adapter le dossier
- je réadapte la reponse si elle est un peu décallée par rapport au projet que je souhaite présenter
- si la réponse est complètement hors sujet vis à vis du projet, je le notifie ("hors propos") et n'y répond pas

## Catégories de questions

### Activités des élèves

**Comment paramétrer les règles de confidentialité du GPS ?**
- hors propos

**Quel est le rendu de la séance 1 ? la séance 2 ?**
- Il n'y a pas de rendu prévu (pour l'instant ?) à la fin de la séance 1 du projet (qui correspond à la séance 3 de la séquence) et la séance 2 du projet (qui correspond à la séance 6 de la séquence). 
- Il y a par contre des traces écrites centralisées dans le dossier individuel de suivi de projet. Ces traces seront évaluées et corrigées vers la fin du projet. 
- Les traces écrites dans la séance 1 sont décrites <a href="supports/dossier-_individuel-_de-_suivis-_de-_projet.md" target="_blank">ici</a>. (⚠️ **TODO** ⚠️ : à compléter) 
- ⚠️ **TODO** ⚠️ : Les traces écrites de la séance 2 du projet sont encore à définir. 

**Quel est le cahier des charges lié au choix de l'itinéraire ?**
- question ré-adaptée : quel est le cahier des charges ? le cachier des charges est rédigé <a href="supports/cahier-_des-_charges.md" target="_blank">ici</a>

**Que faire si un des outils n'est pas disponible ?**
- ⚠️ **TODO** ⚠️ : dans le document de présentation du projet, ajouter un paragraphe listant les outils et proposant des alternatives si ils ne sont pas disponibles

**Que faire pour palier aux difficultés d'un groupe à avancer ?**
- Dans le déroulement de la séance 1 du projet détaillé ici, certaines étapes relèvent cette difficulté possible et comment y remédier
- Les stratégies mises en place dans la séance 1 seront semblables à celles de la séance 2 (encourager l'entre aide entre groupes / placer les groupes en difficulté devant / identifier les groupes en difficulté et leur venir en aide après un certain temps défini dans la fiche de séance / ...)
- Début séance 3 du projet (= séance 9 de la séquence), les binômes seront fusionnés pour former des groupes de 4. Un groupe en difficulté pourra profiter du travail de l'autre binôme pour avancer.
- ⚠️ **TODO** ⚠️ : définir les stratégies à adopter pendant la phase 3 (qui s'étale sur 3 séances) en tenant compte du fait que les groupes seront beaucoup plus mis en autonomies

**A quelle fréquence le projet doit-il avancer / doit-on vérifier l'avancement du projet ? Pourquoi ?**
- A quelle fréquence le projet doit-il avancer => par rapport à ?
- Doit-on vérifier l'avancement du projet ? Pourquoi ? => oui, pour s'assurer ques les groupes avancent et éviter que de trop gros écart se creusent entre les groupes 

### Pédagogie de projet

**Justifier le choix des outils utilisés lors du projet**
- Dans le <a href="supports/cahier-_des-_charges.md" target="_blank">cahier des charges</a> sont explicités les outils et le choix d'utilisation (⚠️ **TODO** ⚠️ : hormis pour le raspberry)

### Connaissances et compétences travaillées

**Demander si l'algo de Dijkstra fait partie du prog de seconde? si négatif, dans quel programme est il ?** (#question-piege,#il_est_pas_au_programme_de_lycée<- ro le spoiler !)
- question ré-adaptée : s'assurer que les pré-requis du projet et les connaissances à mettre en oeuvre sont à la portés des élèves
    - les prérequis (à compléter peut être?) sont listés dans la fiche récapitulative du document <a href="contruction-_du-_dossier.md#fichercapitulative" target="_blank">contruction du dossier</a>
    - Les principales connaissances à avoir sont issues du programme de 1ère NSI. Et le contenu du thème "Base de données" dépend très peu des autres thèmes de terminale.

### Différentiation pédagogique

**Quelles difficultés les élèves peuvent rencontrer => comment est gérée la différentiation pédago ?**
- Je viens en aide aux groupes en difficulté dans les phases 1 et 2. L'entre aide entre groupe est également fortement encouragée
- Durant la phase 3 où les elèves évoluent en groupes avec une plus grande autonomie, le projet gagne en modularité. Ainsi, les groupes pourront se répartir les rôles sur différentes parties du projet en fonction des affinités/difficultés. (en reflexion) Il est possible que j'intervienne directement dans la répartition des rôles si besoin.

**Que prévoir si un élève/groupe fini en avance ?**
- La séance 1 du projet (phase 1 du projet / séance 3 de la séquence) décrite de manière détaillée ici explicite ces cas de figures dans <a href="seances/seance-_3.md#droulementdelasance" target="_blank">les déroulements de la séance</a>
- La séance 2 du projet (phase 2 du projet / séance 6 de la séquence) adoptera des stratégies similaires
- La phase 3 du projet, les groupes évoluront plus en autonomies. Un ensemble de fonctionnalités optionnelles sont proposées dans le <a href="supports/cahier-_des-_charges.md#fonctionnalitsoptionnelles" target="_blank">cahier des charges</a> et les groupes sont également encouragés à proposer des idées de fonctionnalités supplémentaires à développer

**Comment éviter au maximum la différentiation pédagogique ?**
- redondant avec les questions/réponses précédantes 

### Evaluation des travaux des élèves

**Donner des exemples de questions possibles du quiz**
- question ré-adaptée : donner un exemple de questions dans l'évaluation sommative si il y a
    - => ⚠️ **TODO** ⚠️ : une évalutation sommative est prévue vers la fin de la séquence, rédiger quelques exemples de questions (QCM ou non)

**Justifier la cohérence du type d'évaluation choisi pour l'activité/le projet concerné**
- L'évaluation est découpée comme suit : (les nombres de points sont toujours en cours de réflexion)
    - le contenu du dossier individuel de suivi de projet (~ 6 points sur 20)
    - la présentation en groupe du projet (~1 points sur 20)
    - le contenu du projet final (~3 points sur 20)
    - une évaluation sommative (~10 points sur 20)
- La priorité est mise sur le travail et la compréhension individuels des élèves. L'évaluation sommative sera logiquement plus théorique que le projet.

**Proposer une alternative cohérente au type d'évaluation choisi.**
- ⚠️ **TODO** ⚠️ : à réfléchir, pas d'idée pour l'instant

### Enjeux sociétaux 

**L'utilisation de GPS pouvant retracer nos moindres faits et gestes ne constitue-t-il pas une violation de la vie privée ?**
- hors propos

**A quoi peuvent servir les données de localisation collectées ?**
- hors propos

**Justifier que le matériel nécessaire à l'activité ne stigmatise pas les élèves issus de famille à revenu faible.**
- hors propos

=> Dans le projet que je propose, les enjeux sociétaux gravitent d'avantages autour des questions liées à la santé publique et à l'écologie (activite humaine impactant la qualité de l'air)

### Système éducatif

**Comment adapter le projet si un élève en situation de handicap ne peut pas facilement se déplacer?**
- hors propos

**Comment faire pour mettre au travail un élève récalcitrant à l'activité/au projet?**
- travailler sur le teasing du thème (pas évident, mais possible !) et le teasing du projet. Mettre en avant la collaboration entre élèves + la possibilite d'apporter ses idées (phase 3 projet) +  la mise en oeuvre d'une architecture complexe, fruit de deux années d'enseignement au lycée + ...

### Métier d'enseignant

**Estimer le temps nécessaire pour preparer le projet en amont (verifier le matériel, la dispo, les applications, ...)**
- ⚠️ **TODO** ⚠️ : Prévoir un raspberry de secours, ajotuer cette info dans le document de présentation de projet

**Estimer la liste du matériel nécessaire afin de réaliser l'activité/le projet**
- estimé quoi donc ? 

**Estimer le cout du matériel nécessaire .**
- la liste du matériel : 
    - un raspberry 4 8Go avec chargeur: ~ 90e
    - un module 'sensor hat' : ~ 41e
    - un module 'nova SDS011' : ~ 20e
    - une carte SD 64Go : ~12e
    - protections diverses 
    = ~170e + raspberry de secours ~= 260e 
- sachant que le raspberry + sensor hat pourront être ré-utiliser dans d'autres parties du programme de NSI
- 
⚠️ **TODO** ⚠️ ajouter cette liste / estimation quelque part dans le document de présentation du projet