# 31 Mars

## retour analyse des dossiers

intervenants : Camille / jean-francois bégot

absence de contenu pedagogique
- faut rentrer dans le concret 
    
pour la semaine pro : preparer un TD 

concretement comment leleve peut arriver aux objectifs

## ensuite , creation de groupe

travail sur padlet : https://padlet.com/carmand_fde/aj18y14m8vwmex4w

- quel doc a dispo pour les eleves 
- comment accompagner les eleves / les aider 

concevoir une trame 
axe de vue projet + axe de vue apprentissage 



## APREM 

intervenants : cecile raynal / jean-francois bégot

question jury previsibles : https://i.ibb.co/Dp6f3t5/Screenshot-from-2021-03-31-13-35-46.png

deux activités : https://i.ibb.co/DLCZqmN/Screenshot-from-2021-03-31-13-36-55.png

travail sur framapad : https://annuel2.framapad.org/p/questionsoral2-9mm9?lang=fr

act 1 : trouver differentes questions de jury catégorisées pour le projet https://canabae.enseigne.ac-lyon.fr/spip/spip.php?article1427
act 2 : repondre aux questions de act 1 avec son projet 

correction 
- act 1
    - eviter de caller des trop pas exploité (confidentialite sur portable dans ce projet)
    - loi > regleemnt interieur > equipe pedagogique 
    - sassurer que les eleves passent bien par les points prevus (trace ecrite ?) / sassurer que les eleves qi compris ce qui est prevu 
        - prevoir seance remplacement tiré du sac si panne doutils
    - la differenciation pedago : gerer ceux qui avancent MAIS AUSSI ceux qui sont a la bourre
    - differentiation deleves dys et eleves a besoin pedagogique (meme si dys peut avoir besoin pedago particulier)
    - quizz
        - on evalue des connaissances 
            - compliqué devauler des competences 
        - eleves mauvaise volonté 
            - faut anticiper
            - adapter le jour j 
                - faire un diagnostic 
            - phase de decrochage ? donc intervention vie cscolaire / cpe / plan / etc...
            