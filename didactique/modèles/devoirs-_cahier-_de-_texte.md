# Cahier de texte

## Modèle à copier/coller dans le cahier de texte "ecoleDirecte" : 



<pre><code><style>
  .bloc-content ul{
    list-style:revert !important;
    padding: revert !important;
  }
</style>

<!-- devoirs maisons -->
<h3><b><u>Module 01</u>, cours "Introduction en programmation Web côté client" : </b></h3>

<ul>
  <li>partie du cours : "<b>L'architecture client-serveur</b>" :<ol start="0">
    <li>relire le cours</li>
    <li>faire l'activité #0 (les réponses aux questions avec ✍️ sont à écrire dans votre cours)</li>
    <li>recopier dans votre cours le bloc "résumons" :></li>
  </ol></li>
</ul>

<ul>
  <li>partie du cours : "<b>À la (re?)découverte du langage HTML</b>" :<ol start="0">
    <li>relire le cours</li>
    <li>faire l'activité #1 (les réponses aux questions avec ✍️ sont à écrire dans votre cours)</li>
    <li>recopier dans votre cours le bloc "résumons" :></li>
  </ol></li>
</ul>

<!-- contrôle -->
<h3><b><u>Contrôle</u>, module/chapitre 01 "Introduction en informatique fondamentale et introduction en programmation Web côté client": </b></h3>

<ul>
  	<li><b>durée du contrôle</b> : 1 heure</li>
    <li>lien vers les cours</li>
    <li>lien vers les exercices de perfectionnement (en cours de rédaction)</li>
    <li><b>contenu du contrôle</b> : <ul>
      <li>il y aura des questions de cours sous forme de QCM : une question par bloc 'résumons' du module 01 et une question supplémentaire pour chaque module précédent</li>
      <li>il y aura un à plusieurs exercices en lien avec le module 01</li>
      <li>il y aura un à plusieurs exercices de programmation sur les parties : variables, instructions conditionnelles, instructions itératives, les fonctions (celui-là sera simple)</li>
      <li><b>spécificité(s) du module 01</b> : <ul>
        <li>il faut connaître par cœur les tables de vérités and or et not</li>
      </ul></li>
    </ul></li>
</ul>

</code></pre>