# Fiche de séquence

<style>
table th{
    text-align:center;
}
p,li { 
  text-align: justify;
  text-align-last: left;
}
</style>

<table class="table w-100">
    <tbody>
        <tr>
            <td><b>Place dans l'année scolaire : </b>exemple 'Nième et Nième+1 semaine de février'</td>
        </tr>
        <tr>
            <td><b>Place dans la progression : </b>exemple 'semaine X à semaine Y sur Z'</td>
        </tr>
        <tr>
            <td><b>Nombre de séance(s) : </b>exemple '6 séances (4 * 2h + 2 * 1h)'</td>
        </tr>
    </tbody>
</table>

+++sommaire

## Présentation générale de la séquence


## La séquence en détail

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Capacité(s) visée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr><td>
          <p class="mb-0"><b>Thème :</b> A</p>
          <ul>
            <li>Nom de la capacité</li>
            <li>Nom de la capacité</li>
          </ul>
          <p class="mb-0"><b>Thème :</b> B</p>
          <ul>
            <li>Nom de la capacité</li>
            <li>Nom de la capacité</li>
          </ul>
        </td></tr>
    </tbody>
</table>

<table class="table table-sm">
    <thead>
        <tr>
            <th>Compétence(s) travaillée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">
              +++prof supprimer les parties non concernées      
              <p class="mb-0"><b>Compétences de discipline :</b></p>
              <ul class="mb-0">
                  <li>Analyser et modéliser un problème en termes de flux et de traitement d’informations</li>
                  <li>Décomposer un problème en sous-problèmes, reconnaître des situations déjà analysées et réutiliser des solutions</li>
                  <li>Concevoir des solutions algorithmiques</li>
                  <li>Traduire un algorithme dans un langage de programmation, en spécifier les interfaces et les interactions, comprendre et réutiliser des codes sources existants, développer des processus de mise au point et de validation de programmes</li>
                  <li>Mobiliser les concepts et les technologies utiles pour assurer les fonctions d’acquisition, de mémorisation, de traitement et de diffusion des informations</li>
                  <li>Développer des capacités d’abstraction et de généralisation</li>
              </ul>
              <p class="mb-0 mt-2"><b>Compétences transversales :</b></p>
              <ul class="mb-0">
                  <li>Faire preuve d’autonomie, d’initiative et de créativité</li>
                  <li>Présenter un problème ou sa solution, développer une argumentation dans le cadre d’un débat</li>
                  <li>Coopérer au sein d’une équipe</li>
                  <li>Rechercher de l’information, apprendre à utiliser des sources de qualité, partager des ressources</li>
                  <li>Faire un usage responsable et critique des sciences et technologies numériques</li>
              </ul>
            </td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Aperçu des séances</th>
        </tr>
    </thead>
    <tbody>
        <tr><td>
          <p class="mb-0"><b>Séance n°1</b> : <a href="" target=""><i class="isFile fas fa-file-alt"></i> lien vers la fiche séance</a></p>
          <ul>
            <li>(exemple) nous réinvestissons sur les concepts de représentation de nombre binaire côté machine et en profitons pour aller un peu plus loin : comment une machine traite des nombres négatifs et des nombres réels ?</li>
          </ul>
          <p class="mb-0"><b>Séance n°2</b> : <a href="" target=""><i class="isFile fas fa-file-alt"></i> lien vers la fiche séance</a></p>
          <ul>
            <li>blabla</li>
          </ul>
        </td></tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Évaluation(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr><td>
          <p class="mb-0"><b>Diagnostique : </b></p>
          <ul>
            <li>(exemple) En début de chaque seances, un premier exercice et/ou des échanges sont faits avec les élèves pour réactiver des concepts qui sont des pré-requis pour la séance. Je bénéficie ainsi des avantages de ma progression spiralée. Ce début de cours me permet de jauger d'adapter le temps qui devra être pris collectivement et individuellement avec les élèves dans les activités qui suivent</li>
          </ul>
          <p class="mb-0"><b>Formative : </b></p>
          <ul>
            <li>(exemple) Les activités effectuées, ainsi que leur correction, lors de la séance permet aux élèves de se situer dans leurs apprentissages. Des exercices collectifs et/ou individuels peuvent être ajoutés et à faire pour la prochaine séance</li>
          </ul>
          <p class="mb-0"><b>Sommative : </b></p>
          <ul>
            <li>(exemple) Le projet permettra d'évaluer une partie de cette séquence (traitement des données en table). Pour l'autre thème abordé, le prochain devoir surveillé contiendra des questions en lien avec ce dernier</li>
          </ul>
        </td></tr>
    </tbody>
</table>

## Analyse réflexive de la séquence