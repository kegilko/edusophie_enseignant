# séance 3 : phase 1 du projet
		
<table class="table text-nowrap">
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Séquence</b></td>
            <td colspan="42">Bases de données (NSI Terminale)</td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Séance nº</b></td>
            <td>3</td>
            <td class="text-center" style="width:0"><b>Durée</b></td>
            <td>2h</td>
            <td class="text-center" style="width:0"><b>Classe</b></td>
            <td>Term X</td>
            <td class="text-center" style="width:0"><b>Semaine</b></td>
            <td>Y</td>
        </tr>
    </tbody>
</table>

<table class="table">
  <thead>
    <tr>
      <th class="text-center" scope="col" colspan="2">Programme officiel couvert</th>
    </tr>
    <tr>
      <th scope="col">Contenu</th>
      <th scope="col">Capacités attendues</th>
    </tr>
  </thead>
  <tbody>
    <tr>
        <td><p>Modèle relationnel : <del>relation</del>, attribut, domaine, clef primaire, <del>clef étrangère</del>, schéma relationnel.</p></td>
        <td><p>Identifier les concepts définissant le modèle relationnel.</p></td>
    </tr>
    <tr>
        <td><p>Base de données relationnelle.</p></td>
        <td>
            <p>Savoir distinguer la structure d’une base de données de son contenu.</p>
            <p>Repérer des anomalies dans le schéma d’une base de données.</p>
        </td>
    </tr>
    <tr>
        <td><p>Système de gestion de bases de données relationnelles.</p></td>
        <td><p>Identifier les services rendus par un système de gestion de bases de données relationnelles : persistance des données, <del>gestion des accès concurrents</del>, efficacité de traitement des requêtes, <del>sécurisation des accès</del>.</p></td>
    </tr>
    <tr>
        <td><p><del>Langage SQL : requêtes d’interrogation et de mise à jour d’une base de données.</del></p></td>
        <td>
            <del>
                <p>Identifier les composants d’une requête.</p>
                <p>Construire des requêtes d’interrogation à l’aide des clauses du langage SQL : SELECT, FROM, WHERE, JOIN.</p>
                <p>Construire des requêtes d’insertion et de mise à jour à l’aide de : UPDATE, INSERT, DELETE.</p>
            </del>
        </td>
    </tr>
  </tbody>
</table>

<table class="table">
    <thead>
        <tr><td class="align-middle text-center" colspan="100"><b>Pré-requis</b></td></tr>
    </thead>
    <tbody>
        <tr>
            <td class="align-middle text-center" style="width:0" rowspan="4"><b>Savoir</b></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>être</b></td>
            <td>
                <span class="text-red">à compléter</span>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>faire</b></td>
            <td>
                <ul>
                    <li>Savoir exécuter des lignes de commande</li>
                    <li>Savoir créer une base de données depuis un logiciel de gestion de bases de données</li>
                    <li>Savoir créer des tables dans une base de données depuis un logiciel de gestion de bases de données</li>
                    <li>Savoir ajouter des champs dans une table dans une base de données depuis un logiciel de gestion de bases de données</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>associé</b></td>
            <td>
                <ul>
                    <li>Savoir différencier un client d'un serveur</li>
                    <li>Savoir définir ce qu'est un protocol</li>
                </ul>
            </td>
        </tr>
            <tr>
            <td class="text-center" style="width:0"><b>Autre</b></td>
            <td colspan="42">
                <ul>
                    <li>(séance 2) avoir déterminé son binôme (choisi par les élèves)</li>
                    <li>(séance 2 -> séance 3) travail élève : avoir parcouru le cahier des charges</li>
                    <li>(séance 2 -> séance 3) travail élève : préparer des questions concernant le cahier des charges</li>
                    <li>(séance 2 -> séance 3) travail élève : remplir la fiche "fiche qualité air"</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

<table class="table">
    <thead>
        <tr><td class="align-middle text-center" colspan="100"><b>Objectifs</b></td></tr>
    </thead>    
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Séquence</b></td>
            <td>
                <b>Connaissance(s)</b>
                <ul>
                    <li class="text-red">à compléter</li>
                </ul>
                <b>Technique(s)</b>
                <ul>
                    <li>concevoir une base de données / rédiger un schéma relationnel</li>
                    <li>créer une base de données SQL</li>
                    <li>peupler une base de données SQL</li>
                    <del>
                        <li>extraire les informations d'une base de données SQL</li>
                    </del>
                </ul>
                <b>Compétence(s) transversale(s)</b>
                <ul>
                    <li>faire preuve d’autonomie, d’initiative et de créativité</li> 
                    <li>développer une argumentation dans le cadre d’un débat</li>
                    <li>coopérer au sein d’une équipe dans le cadre d’un projet</li>
                    <li>rechercher de l’information, partager des ressources</li>
                </ul>
                <b>Dimension(s) environnementale(s)</b>
                <ul>
                    <li>connaitre les principaux facteurs liés à la qualité de l'air en France</li>
                    <del><li>établir des corrélations (si il y a) entre la température, la pression atmosphérique ou encore le taux d'humidité avec le taux de particules fines</li></del>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Séance</b></td>
            <td>
                <ul>
                    <li>utiliser la ligne de commande dans un terminal</li>
                    <li>se connecter sur un serveur distant à travers le protocol SSH</li>
                    <li>utiliser des commandes Linux</li>
                        <ul>
                            <li>lister les commandes disponibles</li>
                            <li>afficher les aides des commandes disponibles (-h ou -help ou man commande)</li>
                        </ul>
                    <li>utiliser un logiciel de gestion de base de données</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

<table class="table">
    <thead>
        <tr><td class="align-middle text-center" style="width:0" colspan="100"><b>Environnement à préparer</b></td></tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Aménagement de l'espace</b></td>
            <td>
                <ul>
                    <li>un aménagement de la classe importe peu (en rangés, en îlots, ...). Les élèves doivent pouvoir se placer en binôme</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Hardware</b></td>
            <td>
                <ul>
                    <li>un Raspberry PI avec connecté dessus les modules "sensor hat" et "nova SDS011"</li>
                    <li>chaque binôme doit disposer d'au moins un poste informatique sur windows</li>
                    <li>les postes et le raspberry PI doivent êtres connectés au même réseau</li>
                    <li>une étiquette sur le Raspberry PI avec marqué dessus son IP (qui sera une IP fixe)</li>
                    <li>un rétro-projecteur sur lequel est branché l'ordinateur de l'enseignant</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Software</b></td>
            <td>
                <b>Poste élève</b>
                <ul>
                    <li>avoir putty installé : logiciel qui permettra de se connecter au raspberry depuis un poste sur Windows</li>
                    <li>avoir DB Browser installé : logiciel qui permettra aux élèves de créer / modifier / peupler la base de données à travers une interface graphique</li>
                </ul>
                <b>Raspberry PI</b>
                <ul>
                    <li>avoir des données disponibles enregistrées à l'aide des modules "sensor hat" et "nova SDS011"</li>
                    <li>créer une commande pour le module "sensor hat" permettant d'afficher des données collectées. La commande affiche un résultat sous la forme 'xx;yy;zz'. Le '-help' doit afficher les valeurs des données affichees.</li>
                    <li>créer une commande pour le module "nova SDS011" permettant d'afficher des données collectées. La commande affiche un résultat sous la forme 'xx;yy;zz'. Le '-help' doit afficher les valeurs des données affichees.</li>
                    <li>créer un compte utilisateur (si non créé jusqu'à là) pour chaque élèves isolé et sécurisé vis à vis du raspberry et des autres comptes. Limiter la RAM utilisable par utilisateurs (100Mo max ?)</li>
                </ul>
                Après s'être connecté au raspberry, un message d'accueil doit afficher la commande permettant de connaitre la liste des commandes disponibles. Le message explique également comment afficher l'aide des commandes (soit via man, soit via commande -h ou commande -help)
            </td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Supports pour l'élève</b></td>
            <td>
                <ul>
                    <li>fournis fin séance 2 : <a href="../supports/cahier-_des-_charges.md" target="_blank">cahier des charges du projet</a> en format numérique</li>
                    <li>fournis en début de séance : <a href="../supports/dossier-_individuel-_de-_suivis-_de-_projet.md" target="_blank">dossier individuel de suivis de projet</a> en format papier</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

<span class="pagebreak">
### Déroulement de la séance

#### En résumé 

Le projet sera tout d'abord présenté.

Ensuite, le coeur de la séance se découpe en 3 principales étapes : 
1. Les binômes doivent se connecter en ssh sur le raspberry et trouver les commandent permettant d'afficher les données récoltées par les modules 'sensor hat' et 'sds011'
2. Les binômes doivent analyser ces données et concevoir une base de données adaptées pour les stocker
3. Les binômes doivent créer la base de données et y insérer manuellement quelques données

#### En détaillé 

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap"><b>#0</b></td>
            <td colspan="3">Placement des élèves à leur arrivé</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h00 -> 00h10</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10">Les élèves sont placés dans la salle avec leur binôme choisi lors de la seance précédente</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0" rowspan="3"><b>activité</b></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>enseignant</b></td>
            <td colspan="10">
                <ul>
                    <li>les binômes composés d'élèves en difficultés sont placés en priorité devant la classe</li>
                    <li>l'appel est fait au fur et à mesure des placements</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>élève</b></td>
            <td class="bg-lightGray" colspan="10"></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support pédagogique</b></td>
            <td class="bg-lightGray" colspan="10"></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td class="bg-lightGray" colspan="10"></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>connaissances<br>/<br>compétences</b></td>
            <td class="bg-lightGray" colspan="10">
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>trace(s) écrite(s)</b></td>
            <td class="bg-lightGray" colspan="10"></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10">
                <ul>
                    <li>gestion des retardataires : ?</li>
                    <li>élève sans binôme : sera intégré à un binôme choisis avec soin par l'enseignant</li>
                <ul>
            </ul></ul></td>
        </tr>
    </tbody>
</table>

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap"><b>#1</b></td>
            <td colspan="3">Synthèse de la fiche "qualité de l'air"</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h10 -> 00h25</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10">
                <ul>
                    <li>trois élèves tirés au sort sont amenés au tableau</li>
                    <li>ces élèves doivent discuter de leurs recherches vis à vis du contrôle de la qualité de l'air ('<a href="../supports/fiche-_--_-_qualite-_de-_l--__air.md" target="_blank">fiche : qualité de l'air</a>' à compléter pour cette séance) et rédiger une synthèse au tableau</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0" rowspan="3"><b>activité</b></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>enseignant</b></td>
            <td colspan="10">
                <ul>
                    <li>les retardataires à partir d'ici sont directement envoyés au tableau </li>
                    <li>désigner parmi les trois élèves celui qui aura pour rôle d'animer la discussion, de répartir la parole et de synthétiser les résultats de la discussion au tableau</li>
                    <li>encourager les élèves à se tourner vers la classe pendant les discussions et à interagir avec elle si besoin</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>élève</b></td>
            <td colspan="10">
                <ul>
                    <li>s'avancer au tableau lorsqu'on est désigné</li>
                    <li>le reste de la classe répond aux éventuelles questions posées par les trois élèves</li>
                    <li>une fois la discussion terminée et synthétisée au tableau. Les 3 élèves regagnent leur place. Puis tous les élèves mettent à jour la fiche '<a href="../supports/fiche-_--_-_qualite-_de-_l--__air.md" target="_blank">fiche : qualité de l'air</a>'</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10">Le tableau de classe</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>connaissances<br>/<br>compétences</b></td>
            <td colspan="10">
                <b>Compétence(s) transversale(s)</b>
                <ul>
                    <li>développer une argumentation dans le cadre d’un débat</li>
                    <li>rechercher de l’information, partager des ressources</li>
                </ul>
                <b>Dimension(s) environnementale(s)</b>
                <ul>
                    <li>Connaitre les principaux facteurs liés à la qualité de l'air en France</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>trace(s) écrite(s)</b></td>
            <td colspan="10">Le fiche '<a href="../supports/fiche-_--_-_qualite-_de-_l--__air.md" target="_blank">fiche : qualité de l'air</a>' qui sera complétée à l'issue des échanges</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10">
                <b>Un élève dans le groupe des désignés n'a pas fait effectué les recherches chez lui</b>
                <ul>
                    <li>Il aura le rôle d'animateur/synthétiseur</li>
                </ul>
                <b>Les élèves du groupe des désignés n'ont globalement pas effectué les recherches chez eux</b>
                <ul>
                    <li>en demander les raisons</li>
                    <li>les élèves du groupe questionnent le reste de la classe et synthétisent les informations à tour de rôle au tableau</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>


<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap"><b>#2</b></td>
            <td colspan="3">Amorce de la phase 1 du projet</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h25 -> 00h45</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10">
                <ul>
                    <li>présentation détaillée du projet</li>
                    <li>présentation du dossier individuel "suivis de projet"</li>
                    <li>sessions questions / réponses sur le cahier des charges</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0" rowspan="3"><b>activité</b></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>enseignant</b></td>
            <td colspan="10">
                <ul>
                    <li>(ré-)exposer le but du projet</li>
                    <li>expliquer le lien entre la séquence actuelle et le projet</li>
                    <li>encourager les questions et y répondre</li>
                    <li>expliquer l'objectif de la phase 1 du projet en s'appuyant sur la partie 'Planification' du <a href="../supports/cahier-_des-_charges.md" target="_blank">cahier des charges du projet</a></li>
                    <li>faire distribuer un dossier individuel "suivis de projet"</li>
                    <li>détailler les conditions d'évaluations individuelles qui sont explicitées dans le <a href="../supports/dossier-_individuel-_de-_suivis-_de-_projet.md" target="_blank">dossier individuel de suivis de projet</a></li>
                    <li>sessions questions / réponses sur le <a href="../supports/cahier-_des-_charges.md" target="_blank">cahier des charges du projet</a> et le <a href="../supports/dossier-_individuel-_de-_suivis-_de-_projet.md" target="_blank">dossier individuel de suivis de projet</a></li>
                    <li>encourager les élèves à ajouter des notes dans la page "notes personnelles" du <a href="../supports/dossier-_individuel-_de-_suivis-_de-_projet.md" target="_blank">dossier individuel de suivis de projet</a></li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>élève</b></td>
            <td colspan="10">alterner entre écoutes, questions/reponses et prises éventuelles de notes dans les pages "notes personnelles" du <a href="../supports/dossier-_individuel-_de-_suivis-_de-_projet.md" target="_blank">dossier individuel de suivis de projet</a></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support pédagogique</b></td>
            <td colspan="10">
                <ul>
                    <li>le <a href="../supports/cahier-_des-_charges.md" target="_blank">cahier des charges du projet</a></li>
                    <li>le <a href="../supports/dossier-_individuel-_de-_suivis-_de-_projet.md" target="_blank">dossier individuel de suivis de projet</a></li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10">Le tableau de classe si besoin</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>connaissances<br>/<br>compétences</b></td>
            <td class="bg-lightGray" colspan="10">
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>trace(s) écrite(s)</b></td>
            <td colspan="10">les pages "notes personnelles" du <a href="../supports/dossier-_individuel-_de-_suivis-_de-_projet.md" target="_blank">dossier individuel de suivis de projet</a></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10">
                <ul>
                    <li>une majorité d'élèves appréhende la partie web du projet</li>
                    <ul>
                        <li>insister sur le fait que le code du serveur HTTP sera fournis et qu'il suffira de le compléter</li>
                        <li>attendre que les difficultées emergent </li>
                    </ul>
                    <li>une majorité d'élèves ne prend aucune note</li>
                    <ul>
                        <li>faire répéter la réponse à certaines questions à des élèves</li>
                        <li>si les réponses ne sont pas satisfaisantes, insister sur les points semblant poser problèmes et la prise de note</li>
                    </ul>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap"><b>#3</b></td>
            <td colspan="3">Projet "Phase 1" : jeux de piste avec les commandes</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h45 -> 01h15</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10">
                Les groupes, laissés en autonomie, doivent réussir à se connecter sur le raspberry et à afficher des données provennant des modules "sensor hat" et "nova SDS011".
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0" rowspan="3"><b>activité</b></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>enseignant</b></td>
            <td colspan="10">
                <ul>
                    <li>annoncer le depart officiel du projet</li>
                    <li>laisser les groupes avancer en autonomie</li>
                    <li>pendant cette étape, si besoin, rappeler aux élèves de noter les informations leur semblant utiles dans les pages "notes personnelles" du <a href="../supports/dossier-_individuel-_de-_suivis-_de-_projet.md" target="_blank">dossier individuel de suivis de projet</a></li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>élève</b></td>
            <td colspan="10">Les élèves, en groupe, s'aident de leurs connaissances et du <a href="../supports/cahier-_des-_charges.md" target="_blank">cahier des charges du projet</a> pour entamer le projet</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support pédagogique</b></td>
            <td colspan="10">
                <ul>
                    <li>le <a href="../supports/cahier-_des-_charges.md" target="_blank">cahier des charges du projet</a></li>
                    <li>la fiche '<a href="../supports/fiche-_--_-_qualite-_de-_l--__air.md" target="_blank">fiche : qualité de l'air</a>'</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10">
                <ul>
                    <li>les postes informatiques des élèves (client)</li>
                    <li>le raspberry et ses modules (serveur)</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>connaissances<br>/<br>compétences</b></td>
            <td colspan="10">
                <ul>
                    <li>utiliser la ligne de commande dans un terminal</li>
                    <li>se connecter sur un serveur distant à travers le protocol SSH</li>
                    <li>utiliser des commandes Linux</li>
                        <ul>
                            <li>lister les commandes disponibles</li>
                            <li>afficher les aides des commandes disponibles (-h ou -help ou man commande)</li>
                        </ul>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>trace(s) écrite(s)</b></td>
            <td colspan="10">Les pages "notes personnelles" du <a href="../supports/dossier-_individuel-_de-_suivis-_de-_projet.md" target="_blank">dossier individuel de suivis de projet</a></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10">
                <ul>
                    <li>certains groupes terminent cette étape avant les autres : les mobiliser pour aider les groupes en retards, les briefer sur comment aider efficacement (ex: ne jamais donner la réponse)</li>
                    <li>pour une raison indépendante aux élèves, la connexion avec le raspberry et/ou la récupération des données ne fonctionne pas et le problème ne se résoudra pas rapidement : fournir une clé usb à la classe contenant des exemples de sortie des commandes affichants les données des modules "sensor hat" et "nova SDS011"</li>
                    <li>les élèves ne trouvent toujours pas après un certain temps comment se connecter en ssh sur le raspberry : proposer aux groupes plus avancés de les aider et sinon si nécessaire l'enseignant présente sur le rétro projecteur un exemple d'utilisation du logiciel Putty</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap"><b>#4</b></td>
            <td colspan="3">Projet "Phase 1" : conception et création de la base de données</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">01h15 -> 02h00</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10">Les groupes doivent analyser les données collectées par les modules "sensor hat" et "nova SDS011" et rédiger un shéma relationnel adaptés. Une fois terminé, les groupes doivent créer la base de données depuis le logiciel 'DB Browser' et y saisir quelques données</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0" rowspan="3"><b>activité</b></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>enseignant</b></td>
            <td colspan="10">
                Laisser les groupes avancer en autonomie
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>élève</b></td>
            <td colspan="10">Les élèves, en groupe, s'aident de leurs connaissances et du <a href="../supports/cahier-_des-_charges.md" target="_blank">cahier des charges du projet</a> pour entamer le projet</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support pédagogique</b></td>
            <td colspan="10">
                <ul>
                    <li>le <a href="../supports/cahier-_des-_charges.md" target="_blank">cahier des charges du projet</a></li>
                    <li>la fiche '<a href="../supports/fiche-_--_-_qualite-_de-_l--__air.md" target="_blank">fiche : qualité de l'air</a>'</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10">
                <ul>
                    <li>les postes informatiques des élèves (client)</li>
                    <li>le raspberry et ses modules (serveur)</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>connaissances<br>/<br>compétences</b></td>
            <td colspan="10">
                <b>Technique(s)</b>
                <ul>
                    <li>concevoir une base de données / rédiger un schéma relationnel</li>
                    <li>créer une base de données SQL</li>
                    <li>peupler une base de données SQL</li>
                </ul>
                <b>Compétence(s) transversale(s)</b>
                <ul>
                    <li>faire preuve d’autonomie, d’initiative et de créativité</li> 
                    <li>coopérer au sein d’une équipe dans le cadre d’un projet</li>
                    <li>rechercher de l’information, partager des ressources</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>trace(s) écrite(s)</b></td>
            <td colspan="10">
               <ul>
                    <li>les pages "notes personnelles" du <a href="../supports/dossier-_individuel-_de-_suivis-_de-_projet.md" target="_blank">dossier individuel de suivis de projet</a></li>
                    <li>la partie "Phase 1" du <a href="../supports/dossier-_individuel-_de-_suivis-_de-_projet.md" target="_blank">dossier individuel de suivis de projet</a></li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10">
                <ul>
                    <li>certains groupes n'ont pas réussis à passer l'étape précédente : prendre ces groupes à part pour les aider et laisser les autres continuer.</li>
                    <li>certains groupes terminent cette étape avant les autres : les mobiliser pour aider les groupes en retards</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table></span>