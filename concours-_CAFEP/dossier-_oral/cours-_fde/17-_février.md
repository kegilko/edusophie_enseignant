# 17 février

+++ sommaire

## matin 

PFA : Prof de lycée enseiangnt en université, désigné par l'inspection pour accompagner prof stagaire

PRAG : enseignant quen université 

moodle université : oral 2 NSI 2021 moodle cours cette aprem

Document officiel sur contenu oral 2 : 
    "Le jury attend des dev personnels approfondis de nature disciplinaire" : 
        faut que sa soit connecté avec les eleves 
        
simulation en condition : debut en Mai 

regarder commnent un truc fonctionne > pedagogie inductive

regarder les ressources NSI sur edulcol :
- https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-gt

quon fait les eleves avant, comment les evaluer ? il faut surtout determiner ce quon va faire avec lkes eleves, ce quon leur donne 

penser aux iff profits des eleves / susciter l'interet / ...

## aprem 

Discussions 

dispositifs = choix de l'organisation , 2 ex : jorga ma classe pour faire un exo isolé , ou alors je prevoie une séance d'une heure et demai avec plusiewurs exercices ou chacun avance aavec des rythmes diffs

dispositifs pedago : 
- cours magistral
- classes inversées : on transfert la partie cours a la maisons (avant session en classe)
    - capsule video 
    - activité sur l'ENT (travaux, QCM...)
    - regarder un documentaire/film sur le sujet
- dispositif du fil rouge (réalisation (et non projet a priori) qui sétale tout au long de l'année)

Exercices VS TD : 

plan de travail : 
    on sooumet aux eleves un ensemble d'exo , il choisit ses exos . En fonction de sa reussite . demander aux autres, aider aux autres, demander au profs, etc etc....

TP vs projet : temps cours (TD) / temps plus long (projet)

déamrche inductive :analyser un existant et degager des regles 

artefact, qques defs : 
- quand on utilise qque chose en dehors ce quon vise pour permettre dobserver les regles de fonctionnement . Ex , si on est sur algo de trim, un moment donnée sa vaut le coup de le coder (et le langage la est un 'artefact')
- un moyen non necessaire afin d'atteindre un but necessaire ?
- autre exemple : but recherché est maitrise de lortographe et word est un rtefact 
- autre exemple : utiliser un robot lorsque les eleves travaillent sur les equations logiques 

TP sera plus amménagé que le projet (plus encacdré par le prof)
L'élève a pluss de décisions a prendre pendant le projet 

projet : on decris ce que dois faire la solution , mais pas les etapes 
TP : on decris les étapes 

les vidées pedagogiques , difference entre TP et Projet : 
- TP on travaille pas les meme competences si on est en TP ou si on est en Projet : 

ce qui est present dans les projet et pas dans les TP : 
- projet : repartition des roles , on travaille la conception profonde 
- TP : tous le monde fait la meme chose, on vise une meme connaissance commune avec tous les eleves. l'éleve accede a la connaissance conventionnelle

Questce quon peut viser avec les TP et quon ne peut pas viser pendant les projets ? 
on va acceder a leleve une connaissance communément connue



Synthese : 
- point tres importat : TD/TP/cours majistral/ ... , c'est classique. Par contre, pédagogie par projet, les jury attendent au tournant 
- ne pas trop trop fermer le perimetre de projet dans une direction ; beaucoup davis différents entre les profs / jury / ...

##### Travail sur des trames 

On travaille sur 3 trames de dossiers ; 

###### Trame 1 

Lien : disponible sur le discord de la promo (trame 1)

- jeu de piste : on voit pas trop si c'est un projet ou TP 
- Il s'agit d'un autre dispositif. 
- => critique : les liens avec le programme sont a clarifier
- => critique : parle de projet dès le début, mais ne propose pas vraiment de projet
- on defini pas les doc données aux élèves (a faufiner)



###### Trame 2

Lien : disponible sur le discord de la promo (trame 2)

- on sait pas trop ce quil va laisser a charge aux élèves 
- pas suffisamment décrit pour expliquer ce que feront les élèves 
- on a du mal a voir le projet la dedans, 
- pas très au clair sur ce qui est demandé aux élèves 
- difficulté trop locale/compliquée (cas de la récursivité pour la découverte des cases) 
- attention aussi : évaluation faite qu'en fin de projet , très (trop) risqué pour les élèves
- 

###### Trame 3 

Lien : disponible sur le discord de la promo (trame 3)

- le squelette de code guide fortement : on perd le coté projet 
- trop guidé = on s'éloigne d'un projet. Ici ça ressemble plus à une succession de TP 
- technique utilisé pour contourner certqines difficultés : certaines parties sont codees par le prof
- travail de definition et de conception assez bon / poussé : le jury aime bien a priori

##### slides de synthese

sociaconstructivisme > competence communication / sociale

faut que le dossier soit centré sur l'élève : prerequi, ce quil doit faire, ce quil apprendre en fin de séance, prevoir la trace écrite

##### Extrait rapport jury CAPES interne 2019

Lien : [ici](https://cdn.discordapp.com/attachments/811530139237089310/811611240190836736/Extraits_de_rapports_de_jury_de_Capes_internes_2019.pdf)

#### pour la suite 

- gratter la trame 
- reflechir a un contenu qui soit a la fois original et en lien avec les prog et qui se prette bien a la pedago projet (par exemple : laisser de la libertés aux eleves pour resolution de probleme et/ou mecanisme de repartition taches / organisation 'interne libre')
- attention la pedago projet nest pas une fin en soit non plus 
- faut faire attention a la phase "libre" : faut une analyse pour le jury sur les differentes strat des eleves 
- attention aussi : complicado pour un premiere de decomposer probleme en sous probleme , et repartir les taches. Faut etre vigilant la dessus


deposer trame, juste le tablo ?, le 27 avant minouit
