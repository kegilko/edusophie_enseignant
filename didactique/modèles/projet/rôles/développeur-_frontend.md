# Rôle : développeur(se) frontend

+++image "/images/md/E57H7EA647DH54G3GH8H5EA93BF84A" col-4 mx-auto

+++citation
  Dans l'industrie informatique, le(la) développeur(se) frontend est un(e) informaticien(e) spécialisé(e) dans la création d'interface graphiques soignées et ergonomiques. Avec de l'expérience, il ou elle est capable de concevoir et de développer des interfaces graphiques animées et qui s'adaptent à tous les appareils (ordinateur, tablette, smartphone, ...)
+++

<div class="text-center h3 my-4">Spécialités du développeur frontend <b>au lycée</b></div>

<ul>
  <li>Développer des interfaces graphiques</li>
  <li>S'assurer que l'expérience de l'utilisateur soit la plus agréable possible</li>
  <li>Choisir les couleurs et images utilisées dans une application ou site Web (charte graphique)</li>
</ul>


<div class="text-center h3 my-4">Technologies utilisées <b>au lycée</b></div>

- **langages** : html, css, javascript
- **formats de fichiers utilisés** : html, css, js, json, csv
- <abbr title="un framework est une sorte de boîte à outils informatiques qui aide à développer une application"><b>frameworks</b></abbr> : 
    - flask (python) : création de site ou application Web
- **divers** : 
    - débogueur du navigateur Web

<div class="text-center h3 my-4">Arbre de progression <b>niveau lycée et au-delà</b></div>

+++bulle matthieu
  étant donné l'émergence quotidienne de nouvelles technologies, il n'est pas évident pour un débutant en informatique de se repérer durant son ascension
+++

Des informaticiens, qui se sont penchés sur cette problématique, élaborent des "roadmap" d'apprentissage sous forme de graphe. Ces "roadmap" permettent de situer visuellement son niveau et de voir quelles sont les technologies qu'il faut aborder par rapport à son niveau afin de progresser efficacement

+++imageFloat /images/md/892A88B4CG9C5839HCFF9E48667HBC
  <b>Arbre de progression 'frontend'</b> : 
  <ul>
    <li><u>début</u> : niveau seconde générale SNT</li>
    <li><u>fin</u> : niveau bac +3/+5</li>
    <li><u>sens de lecture</u> : du haut vers le bas</li>
    <li><u>commentaire</u> : -</li>
    <li><u>lien</u> : +++lien "https://roadmap.sh/frontend" "site roadmap.sh"</li>
  </ul>
+++