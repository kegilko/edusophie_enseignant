# Grille d'évaluation du projet

+++consignes
  <ul start="1">
    <li><b>Pendant le projet : </b><ul>
      <li>Seul les deux premiers tableaux doivent être rempli</li>
      <li><b>Tous</b> les rôles prévus au projet doivent être pourvus ,un rôle peut être partagé entre plusieurs membres de l'équipe</li>
      <li>En cours de projet, vous pouvez permuter de rôle avec un autre membre de l'équipe</li>
    </ul></li>
    <li><b>Lors de la dernière séance en projet</b>, vous devez répartir entre vous un total de 5 points en complétant la troisième colonne "Répartition des 5 points" du tableau "Membres de l'équipe"</li>
    <li><b>À la fin de la dernière séance en projet</b>, vous devez transmettre ce document à l'évaluateur. Si ce document n'est pas rendu complété à l'issue de la dernière séance projet, <b class="text-red">une pénalité finale de 8 points</b> est appliquée à chaque membre de l'équipe</li>
    <li><b>Durant la présentation orale : </b><ul>
      <li>Un support de présentation sous forme de slides est attendu</li>
      <li>Une question orale technique sera posée à chaque membre de l'équipe</li>
    </ul></li>
  </ul>
+++

+++pagebreak

<table class="table table-sm">
  <tbody>
    <tr>
      <td><b>Nom du projet</b></td>
      <td class="col-8"></td>
    </tr>
    <tr>
      <td><b>Nom de l'équipe</b></td>
      <td></td>
    </tr>
  </tbody>
</table>
  

<table class="table table-sm">
  <thead>
    <tr>
      <th class="text-center" colspan="4">Membres de l'équipe</th>
    </tr>
    <tr class="text-center align-middle">
      <th class="col-1">#</th>
      <th class="col-3">Nom Prenom</th>
      <th class="col-3">Rôle</th>
      <th class="col-3">Répartition des 5 points</th>
  </tr></thead>
  <tbody>
    <tr>
      <td class="p-3 text-center"><b>M1</b></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr>
      <td class="p-3 text-center"><b>M2</b></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr>
      <td class="p-3 text-center"><b>M3</b></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr>
      <td class="p-3 text-center"><b>M4</b></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
  </tbody>
</table>

+++pagebreak

## Évaluation

<div class="col-10 mx-auto">
  <table class="table table-sm">
    <thead>
      <tr>
        <th colspan="4" class="text-center">Coefficients</th>
      </tr>
    </thead>
    <tbody class="text-center">
      <tr>
        <td class="col-3"><b>I</b><sub>nsuffisant</sub> | non</td>
        <td class="col-3"><b>P</b><sub>assable</sub></td>
        <td class="col-3"><b>B</b><sub>ien</sub></td>
        <td class="col-3"><b>T</b><sub>rès</sub><b>B</b><sub>ien</sub> | oui</td>
      </tr>
      <tr>
        <td>0</td>
        <td>0.3</td>
        <td>0.7</td>
        <td>1</td>
      </tr>
    </tbody>
  </table>
</div>

### Avant la présentation

<table class="table table-sm">
  <thead>
    <tr>
      <th class="text-center " colspan="6">Contenu du projet (15 points)</th>
    </tr>
  </thead>
  <tbody>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>2 points</b></td>
      <td rowspan="2">Le projet a été rendu <b>48 heures</b> avant la séance de présentation des projets</td>
      <td colspan="2" class="text-center col-1" style="height:23px !important">non</td>
      <td colspan="2" class="text-center col-1">oui</td>
    </tr>
    <tr>
      <td colspan="2" class="p-3"></td>
      <td colspan="2" class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>2 points</b></td>
      <td rowspan="2">Le projet, rendu, est contenu dans une archive et fonctionne tel quel une fois l'archive décompressée (il n'y a pas de dossiers/fichiers à créer ou déplacer après décompression)</td>
      <td colspan="2" class="text-center col-1" style="height:23px !important">non</td>
      <td colspan="2" class="text-center col-1">oui</td>
    </tr>
    <tr>
      <td colspan="2" class="p-3"></td>
      <td colspan="2" class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>3 points</b></td>
      <td rowspan="2">Une documentation de qualité, et <b>en format pdf</b>, est fournie avec le projet rendu. Le contenu de la documentation doit suivre +++lien "https://trophees-nsi.fr/media/pages/preparer-votre-participation/7de7e74b57-1643731581/modele_presentation_projet_openoffice.odt" "le modèle suivant" (qui est le modèle utilisé pour le concours 'trophées NSI')
      </td>
      <td class="text-center" style="height:23px !important">I</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>7 points</b></td>
      <td rowspan="2">Les fonctionnalités principales attendues sont implémentées et fonctionnent sans problème</td>
      <td class="text-center" style="height:23px !important">I</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>1 point</b></td>
      <td rowspan="2">Le code est propre et facilement lisible par une personne extérieure au projet :<ul class="mb-0">
        <li>le code est commenté, surtout les parties qui ont pu poser des difficultés</li>
        <li>le nom des variables et des fonctions est clair</li>
      </ul></td>
      <td class="text-center" style="height:23px !important">I</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
  </tbody>
</table>

+++pagebreak

### Pendant la présentation

<p class="h4 text-center text-blue"><u>Format classique</u></p>

<table class="table table-sm">
  <thead>
    <tr>
      <th colspan="6" class="text-center">Qualité de la présentation (10 points)</th>
    </tr>
  </thead>
  <tbody>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>3 points</b></td>
      <td rowspan="2">Le temps de présentation total (démo comprise) fait entre 9 et 11mn. La démo doit être unique et ne doit pas excéder les 2mn</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;height:23px !important">I</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">P</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">B</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>4 points</b></td>
      <td rowspan="2">La parole est équitablement répartie entre chaque membre du groupe</td>
      <td class="text-center" style="height:23px !important">I</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>3 points</b></td>
      <td rowspan="2">Le support de présentation (slides) est de bonne qualité : <ul class="mb-0">
        <li>Il n'y a pas de fautes d'orthographe ni de fautes de grammaire</li>
        <li>Les slides ne sont pas surchargés en texte, l'utilisation de mots-clés est privilégiée</li>
        <li>Du code est présenté dans les slides, le code n'excède pas 50% du contenu total de tous les slides</li>
        <li>Un slide pour la présentation du projet et pour une conclusion sont présents</li>
      </ul></td>
      <td class="text-center" style="height:23px !important">I</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
  </tbody>
</table>

<p class="h4 text-center text-blue"><b>OU</b></p>

<p class="h4 text-center text-blue"><u>format concours "trophée NSI"</u> (vidéo de ~2 minutes)</p>

<b class="text-red"> /!\ </b> Les critères ci-dessous ne représentent qu'une partie des principaux critères attendus du concours "trophées NSI". La liste exhaustive des critères se trouve dans +++lien "https://trophees-nsi.fr" "le site du concours" <b class="text-red"> /!\ </b>

<table class="table table-sm">
  <thead>
    <tr>
      <th colspan="6" class="text-center">Qualité de la présentation (10 points)  <b class="text-green">(+ 2 points bonus)</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th colspan="6" class="text-center">Réalisation</th>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>2 point</b></td>
      <td rowspan="2">Durant la présentation, la parole est équitablement répartie entre chaque membre du groupe</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;height:23px !important">I</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">P</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">B</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>1 point</b></td>
      <td rowspan="2">La présentation reprend les informations principales de la documentation qui a été fournie avec le projet</td>
      <td class="text-center" style="height:23px !important">I</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>1 point</b></td>
      <td rowspan="2">La présentation inclue une démonstration ou exécution pour attester du fonctionnement du projet</td>
      <td class="text-center" style="height:23px !important">I</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr>
      <th colspan="6" class="text-center">Montage</th>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>1 point</b></td>
      <td rowspan="2">Dans la vidéo sont présents : le nom du projet, le nom de la ville, le nom de l'établissement et le niveau scolaire</td>
      <td colspan="2" class="text-center col-1" style="height:23px !important">non</td>
      <td colspan="2" class="text-center col-1">oui</td>
    </tr>
    <tr>
      <td colspan="2" class="p-3"></td>
      <td colspan="2" class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>2 points</b></td>
      <td rowspan="2">La vidéo dure entre 1mn30 <b class="text-red">minimum</b> et 2mn <b class="text-red">maximum</b></td>
      <td colspan="2" class="text-center col-1" style="height:23px !important">non</td>
      <td colspan="2" class="text-center col-1">oui</td>
    </tr>
    <tr>
      <td colspan="2" class="p-3"></td>
      <td colspan="2" class="p-3"></td>
    </tr>
    <tr>
      <th colspan="6" class="text-center">Esthétique</th>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>1 point</b></td>
      <td rowspan="2">L'image de la vidéo est nette et la luminosité est bien réglée</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;height:23px !important">I</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">P</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">B</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>1 point</b></td>
      <td rowspan="2">Tous les membres du groupes sont compréhensibles, les incrustations sonores sont bien équilibrées</td>
      <td class="text-center" style="height:23px !important">I</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>1 point</b></td>
      <td rowspan="2">La présentation est soignée et les fautes d'orthographes sont corrigées</td>
      <td class="text-center" style="height:23px !important">I</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
  </tbody>
</table>

<hr class="my-5">

<table class="table table-sm">
  <thead>
    <tr>
      <th class="text-center " colspan="6">Question technique individuelle (5 points)</th>
    </tr>
  </thead>
  <tbody>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>5 points</b></td>
      <td rowspan="2">Chaque membre du groupe a su répondre à une question technique</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;height:23px !important">M1</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">M2</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">M3</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">M4</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
  </tbody>
</table>

+++pagebreak

## Bilan

<table class="table table-sm">
  <thead>
    <tr class="text-center">
      <th style="border-left-style:hidden;border-top-style:hidden;"></th>
      <th>M1</th>
      <th>M2</th>
      <th>M3</th>
      <th>M4</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="col-8">Contenu du projet (sur 15 points)</td>
      <td colspan="4"></td>
    </tr>
    <tr>
      <td>Qualité de la présentation (sur 10 points)</td>
      <td colspan="4"></td>
    </tr>
    <tr>
      <td>Répartition des points entre les membres de l'équipe (sur 5 points)</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>Question technique individuelle (sur 5 points)</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr class="border-2">
      <td>Note finale sur 20</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

