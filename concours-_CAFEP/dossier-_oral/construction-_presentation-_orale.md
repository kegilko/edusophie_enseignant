# construction présentation orale

le but de ce document est de rédiger une première réflexion sur la préparation de l'oral 2 du concours 

Ci-dessous, les points attendus me semblant les plus importants : 

- 30mn présentation / 30mn entretien
- mettre en avant dimensions environnementales via projet
- mettre en avant coût du matériel avec réflexion "sur le terrain"
- mettre en avant xp pro (problématique projet ?)
- mettre en avant preparation vis a vis grand oral
- réussir a mettre en avant mon application : activité 2 jointures ? 

## Construction des slides de prez'

### introduction

- présentation perso & pro
- => le pro et le perso m'ont amené face a vous 
- pourquoi j'ai choisi ce thème en particulier

### placer la séquence

- contextualiser la séquence dans la progression
    - glisser un "bien sur avant Mars où se déroulera l'épreuve écrite" 
    - glisser un "pas en début d'années pour laisser la place a un thème plus 'python/programmation'"

- sommaire : présentation courte des activité du dossier
- je fais le choix pendant l'oral de présenter act 1 et projet, et je largumente 

### activité introductive 

- reprendre fin sequence premiere :"traitement des données en table"

suite activité introductive => gogo point historique BDD

### projet "air quality control" 

- contextualiser la séquence dans la progression