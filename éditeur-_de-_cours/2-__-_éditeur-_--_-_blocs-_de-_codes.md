# Éditeur : blocs de codes

+++ sommaire

Il est possible d'inclure à travers l'éditeur des blocs permettant au lecteur d'exécuter directement du code. Le code est exécuté que du côté du client, le serveur n'est aucunément sollicité pendant l'opération.

## Langages supportés 

### Python

Si dans l'éditeur vous écrivez : 

```
widget python
import sys
print(f"la version python supportée est : {sys.version_info}") # affiche la version python supportée par brython
widget
```

Cela produit le résultat suivant :

widget python
import sys
print(f"la version python supportée est : {sys.version_info}") # affiche la version python supportée par brython
widget

Il est également possible d'utiliser la librairie turtle ! Il faut faire comme suit : 

```
widget python

from browser import document
import turtle

turtle.set_defaults(turtle_canvas_wrapper = document['turtle-1'])

skk = turtle.Turtle()
 
for i in range(4):
    skk.forward(50)
    skk.right(90)
     
turtle.done()
widget
```

Ce qui donne : 

widget python

from browser import document
import turtle

turtle.set_defaults(turtle_canvas_wrapper = document['turtle-1'])

skk = turtle.Turtle()
 
for i in range(4):
    skk.forward(50)
    skk.right(90)
     
turtle.done()
widget

<div class="text-red"><b> Attention ! Il ne faut pas oublier les intructions </b></div>
<ul>
    <li><code>from browser import document</code></li>
    <li><code>turtle.set_defaults(turtle_canvas_wrapper = document['turtle-1'])</code> : où '1' correspond à l'id de l'éditeur python visible ici </li>
</ul>

![indication](https://i.ibb.co/BG4jkhJ/Screenshot-from-2021-08-06-16-28-20.png)


### SQL

Si dans l'éditeur vous écrivez : 

```
widget sql
CREATE TABLE character(
    id INTEGER,
    name VARCHAR(100),
    currentLifePoint INTEGER,
    born_on DATE
);

INSERT INTO character(id,name,currentLifePoint,born_on)
VALUES
(1,'harry',27,'1980-07-31'),
(2,'ron',31,'1980-03-01'),
(3,'hermione',27,'1979-09-19'),
(4,'severus',0,'1960-01-09'),
(5,'tom',66,'1926-12-31');

SELECT * FROM character;
SELECT name, born_on 
FROM character 
ORDER BY born_on;
widget
```

Cela produit le résultat suivant :

widget sql
CREATE TABLE character(
    id INTEGER,
    name VARCHAR(100),
    currentLifePoint INTEGER,
    born_on DATE
);

INSERT INTO character(id,name,currentLifePoint,born_on)
VALUES
(1,'harry',27,'1980-07-31'),
(2,'ron',31,'1980-03-01'),
(3,'hermione',27,'1979-09-19'),
(4,'severus',0,'1960-01-09'),
(5,'tom',66,'1926-12-31');

SELECT * FROM character;
SELECT name, born_on 
FROM character 
ORDER BY born_on;

widget

## Limites

Simuler le comportement d'un langage via une librairie javascript ne se fait pas sans concession. 
Il ne sera bien évidemment pas possible de créer par exemple un serveur python à travers votre navigateur !

Pour plus de renseignement sur les possibilités et les limitations, je vous invite à consulter la documentation des librairies utilisées. 
Vous trouverez un lien dans les en-têtes des widgets 'Éditeur'.

## Utiliser le code d'un Éditeur dans un autre Éditeur

Par exemple avec une série de widget 'Éditeur' type SQL, il devient rapidement pénible de devoir copier/coller systématiquement les requêtes de création de table et d'insertion de données. 
Il est possible d'ajouter un ensemble de 'références' en en-tête du code du bloc du widget 'Éditeur'. Voici comment ça fonctionne : 

Si dans l'éditeur vous écrivez : 

```
widget sql (Requête des insertions de données dans la table 'character':2:8:14:1:open) (Requête de création de la table 'character':2:1:6)

-- Puis nous exécutons ce que nous souhaitons ajouter par la suite

SELECT * 
FROM character
WHERE currentLifePoint > 0;

widget
```

Cela produit le résultat suivant :

widget sql (Requête des insertions de données dans la table 'character':2:8:14:1:open) (Requête de création de la table 'character':2:1:6)

-- Puis nous exécutons ce que nous souhaitons ajouter par la suite

SELECT * 
FROM character
WHERE currentLifePoint > 0;

widget

Les paramètres lors de la création d'une référence sont : 

- **widget** langage **(** com **:** widgetRéf\_numéro **:** widgetRéf\_ligne\_debut **:** widgetRéf\_ligne\_fin **:** widgetSource\_ligne\_insertion **:** insertion\_ouverte\_ou\_fermée **)**

où : 

|nom du champ|description|obligatoire ?|valeur par défaut
|--|--|--|
|com|le commentaire du bloc inséré|**oui**|aucune
|widgetRéf\_numéro|le numéro, dans la page, du widget où on souhaite piocher du code|**oui**|aucune
|widgetRéf\_ligne\_debut|la ligne de départ du bloc du widget où on souhaite piocher du code|non|1
|widgetRéf\_ligne\_fin|la ligne de fin du bloc du widget où on souhaite piocher du code|non|infini
|widgetSource\_ligne\_insertion|la ligne dans laquelle on souhaite insérer le code dans le widget|non|1
|insertion\_ouverte\_ou\_fermée|définir si le bloc inséré doit être fermé ('close') ou ouvert ('open')|non|close

# Soon...

Dans les prochaines versions seront implémentées les fonctionnalités/améliorations suivantes : 
- Créer un widget générant une animation de tri en fonction de l'algorithme programmé